package spencer.toish.crawler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.model.Link;

/**
 * Created by doyonghoon on 14. 9. 28..
 */
public class SiteCrawler {

    private List<Link> mLinks = null;

    public List<Link> get() {
        if (mLinks == null) {
            parse();
        }
        return mLinks;
    }

    public String getRawJson() {
        if (mLinks == null) {
            parse();
        }
        JsonObject object = new JsonObject();
        JsonArray linkArray = new JsonArray();

        for (Link l : mLinks) {
            JsonObject link = new JsonObject();
            link.addProperty("title", l.getTitle());
            link.addProperty("address", l.getLink());
            link.addProperty("type", l.getType().getId());
            linkArray.add(link);
        }

        object.add("results", linkArray);
        Gson gson = new GsonBuilder()
//                .setPrettyPrinting()
                .create();
        String json = gson.toJson(object);
        return json;
    }

    private void parse() {
        mLinks = new ArrayList<Link>();
        InputStream in = App.getInstance().getResources().openRawResource(R.raw.doll_sites);
        try {
            final String raw = fromInputStream(in);
            String lines[] = raw.split("\\r?\\n");
            for (String t : lines) {
                String[] split = t.split("http");
                final String title = split[0].trim();
                final String link = "http" + split[1];
                Link l = new Link();
                l.setLink(link.trim());
                l.setTitle(title);
                l.setType(Link.LinkType.DOLL);
                mLinks.add(l);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String fromInputStream(InputStream is) throws IOException {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return null;
        }
    }
}
