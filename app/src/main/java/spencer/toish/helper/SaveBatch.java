package spencer.toish.helper;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.List;

/**
 * Created by doyonghoon on 14. 11. 21..
 */
public class SaveBatch<T extends ParseObject> {

    public interface OnSaveCompleteListener {
        public void onSaveComplete(ParseException e);
    }

    private List<T> mList;
    private int mCount = 0;
    private int mCurrentCount = 1;

    public SaveBatch(List<T> mExtraFields) {
        mList = mExtraFields;
    }

    public void save(final OnSaveCompleteListener listener) {
        if (mList != null && !mList.isEmpty()) {
            mCount = mList.size();
            for (final T obj : mList) {
                obj.pinInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            obj.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        if (mCount > mCurrentCount) {
                                            mCurrentCount++;
                                        } else {
                                            listener.onSaveComplete(e);
                                        }
                                    } else {
                                        listener.onSaveComplete(e);
                                    }
                                }
                            });
                        } else {
                            listener.onSaveComplete(e);
                        }
                    }
                });
            }
        } else {
            listener.onSaveComplete(null);
        }
    }
}
