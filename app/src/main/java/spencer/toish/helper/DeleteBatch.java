package spencer.toish.helper;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import spencer.toish.model.ExtraField;

/**
 * Created by doyonghoon on 14. 11. 21..
 */
public class DeleteBatch {

    public interface OnDeleteCompleteListener {
        public void onDeleteComplete(ParseException e);
    }

    private int mCount = 0;
    private int mCurrentCount = 1;

    private List<ExtraField> mCandidates = new ArrayList<ExtraField>();

    public DeleteBatch() {

    }

    public void delete(final List<String> list, String dollCode, final OnDeleteCompleteListener listener) {
        ParseQuery<ExtraField> query = new ParseQuery<ExtraField>(ExtraField.class);
        query.whereEqualTo("code", dollCode);
        query.findInBackground(new FindCallback<ExtraField>() {
            @Override
            public void done(List<ExtraField> extraFields, ParseException e) {
                if (e == null) {
                    for (ExtraField f : extraFields) {
                        final String code = f.getCode();
                        if (!list.contains(code)) {
                            mCandidates.add(f);
                        }
                    }

                    if (mCandidates != null && !mCandidates.isEmpty()) {
                        mCount = mCandidates.size();
                        for (final ExtraField f : mCandidates) {
                            f.unpinInBackground(new DeleteCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        f.deleteInBackground(new DeleteCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    if (mCount > mCurrentCount) {
                                                        mCurrentCount++;
                                                    } else {
                                                        listener.onDeleteComplete(null);
                                                    }
                                                } else {
                                                    listener.onDeleteComplete(e);
                                                }
                                            }
                                        });
                                    } else {
                                        e.printStackTrace();
                                        listener.onDeleteComplete(e);
                                    }
                                }
                            });
                        }
                    } else {
                        listener.onDeleteComplete(null);
                    }
                } else {
                    listener.onDeleteComplete(e);
                }
            }
        });
    }
}
