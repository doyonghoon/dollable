package spencer.toish.helper;

import android.support.v4.app.FragmentManager;

import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.util.Calendar;

/**
 * Created by doyonghoon on 14. 11. 20..
 */
public final class DatePickerHelper {

    public static final String DATEPICKER_TAG = "datepicker";

    public static void show(FragmentManager manager, DatePickerDialog.OnDateSetListener listener) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
        datePickerDialog.setVibrate(true);
        datePickerDialog.setYearRange(1985, calendar.get(Calendar.YEAR));
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(manager, DATEPICKER_TAG);
    }
}
