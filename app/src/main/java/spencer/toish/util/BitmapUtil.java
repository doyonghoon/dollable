package spencer.toish.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;

import com.parse.ParseFile;

import java.io.ByteArrayOutputStream;

/**
 * Created by doyonghoon on 14. 11. 15..
 */
public class BitmapUtil {

    private static final int MAX_SCALED_BITMAP_SIZE = 1280;

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "share_" + System.currentTimeMillis(), null);
        return Uri.parse(path);
    }

    public static ParseFile convertImageFileToParseFile(String imagePath) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inSampleSize = 2;
        Bitmap b = BitmapFactory.decodeFile(imagePath, opt);
        b = BitmapUtil.scaleDownEfficiently(b);
        return new ParseFile("image_file", BitmapUtil.convertBitmapToBytes(b));
    }

    public static byte[] convertBitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        return stream.toByteArray();
    }

    public static Bitmap convertBytesToBitmap(byte[] byteArray) {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap scaleDownEfficiently(Bitmap bm) {
        return scaleDown(bm, MAX_SCALED_BITMAP_SIZE);
    }

    private static Bitmap scaleDown(Bitmap realImage, float maxImageSize) {
        if (realImage != null) {
            float ratio = Math.min(
                    (float) maxImageSize / realImage.getWidth(),
                    (float) maxImageSize / realImage.getHeight());
            int width = Math.round((float) ratio * realImage.getWidth());
            int height = Math.round((float) ratio * realImage.getHeight());
            return Bitmap.createScaledBitmap(realImage, width, height, true);
        }
        return null;
    }
}
