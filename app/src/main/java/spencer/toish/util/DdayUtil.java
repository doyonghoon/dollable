package spencer.toish.util;

import android.text.TextUtils;

import java.util.Calendar;

/**
 * Created by doyonghoon on 14. 10. 2..
 */
public class DdayUtil {
    public static String calculateDate(String dateStr) {
        if (!TextUtils.isEmpty(dateStr)) {
            String[] split = dateStr.split("/");
            if (split.length < 3) return null;
            String year = split[0];
            String month = split[1];
            String day = split[2];

            if (!TextUtils.isEmpty(year) && !TextUtils.isEmpty(month) && !TextUtils.isEmpty(day) &&
                    TextUtils.isDigitsOnly(year) && TextUtils.isDigitsOnly(month) && TextUtils.isDigitsOnly(day)) {
                int yearInt = Integer.parseInt(year);
                int monthInt = Integer.parseInt(month);
                int dayInt = Integer.parseInt(day);

                Calendar past = Calendar.getInstance();
                past.set(yearInt, getMonth(monthInt), dayInt);

                Calendar now = Calendar.getInstance();
                now.getTimeInMillis();

                long diff = now.getTimeInMillis() - past.getTimeInMillis();
                long days = diff / (24 * 60 * 60 * 1000);
                days += 1;
                return String.valueOf(days + "일");
            }
        }
        return null;
    }

    public static int getMonth(int base) {
        switch (base) {
            case 1:
                return Calendar.JANUARY;
            case 2:
                return Calendar.FEBRUARY;
            case 3:
                return Calendar.MARCH;
            case 4:
                return Calendar.APRIL;
            case 5:
                return Calendar.MAY;
            case 6:
                return Calendar.JUNE;
            case 7:
                return Calendar.JULY;
            case 8:
                return Calendar.AUGUST;
            case 9:
                return Calendar.SEPTEMBER;
            case 10:
                return Calendar.OCTOBER;
            case 11:
                return Calendar.NOVEMBER;
            case 12:
                return Calendar.DECEMBER;
            default:
                return 0;
        }
    }
}
