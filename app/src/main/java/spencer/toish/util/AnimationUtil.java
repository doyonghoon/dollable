package spencer.toish.util;

import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

/**
 * 애니메이션을 쉽게 사용하는 유틸 클래스.
 */
public class AnimationUtil {

    private static boolean startedSecondAnimator = false;

    public static void animateImagery(final Bitmap bm, final ImageView v) {
        final float contrast = 5;
        final float brightness = 255 / 2;
        final Bitmap basic = changeBitmapContrastBrightness(bm, contrast, brightness);
        v.setImageBitmap(basic);

        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(500);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setFloatValues(brightness, 0);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float value = (float) animation.getAnimatedValue();
                v.setImageBitmap(changeBitmapContrastBrightness(bm, contrast, value));

                if (value >= (brightness * 0.9) && !startedSecondAnimator) {
                    WLog.i("start second animator!");
                    // almost finishes..
                    ValueAnimator animator2 = new ValueAnimator();
                    animator2.setDuration(500);
                    animator2.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator2.setFloatValues(contrast, 1);
                    animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation2) {
                            final float value2 = (float) animation2.getAnimatedValue();
                            v.setImageBitmap(changeBitmapContrastBrightness(bm, value2, value));
                        }
                    });

                    if (!startedSecondAnimator) {
                        startedSecondAnimator = true;
                        animator2.start();
                    }
                }
            }
        });
        animator.start();
    }

    private ValueAnimator getContrastAnimator(final float contrast) {
        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(3000);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setFloatValues(contrast, 1);


        return animator;
    }

    private static Bitmap updateSaturation(Bitmap src, float settingSat) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap bitmapResult = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        Canvas canvasResult = new Canvas(bitmapResult);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(settingSat);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
        paint.setColorFilter(filter);
        canvasResult.drawBitmap(src, 0, 0, paint);

        return bitmapResult;
    }

    /**
     * @param bmp        input bitmap
     * @param contrast   0..10 1 is default
     * @param brightness -255..255 0 is default
     * @return new bitmap
     */
    private static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }
}
