package spencer.toish.util;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;

/**
 * Created by doyonghoon on 14. 11. 14..
 */
public class ScreenUtil {
    public static int getWidth(Activity act) {
        return getSize(act, true);
    }

    public static int getHeight(Activity act) {
        return getSize(act, false);
    }

    private static int getSize(Activity act, boolean width) {
        Display display = act.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (width)
            return size.x;
        else
            return size.y;
    }
}
