package spencer.toish.query;

import android.text.TextUtils;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

import spencer.toish.model.Toy;
import spencer.toish.util.WLog;

/**
 * Created by doyonghoon on 14. 11. 17..
 */
public class ToyQuery extends ParseQuery<Toy> {

    public ToyQuery() {
        super(Toy.class);
    }

    public void loadToy(final String objectId, final GetCallback<Toy> callback) {
        whereEqualTo("objectId", objectId)
                .fromLocalDatastore()
                .getFirstInBackground(new GetCallback<Toy>() {
                    @Override
                    public void done(Toy toy, ParseException e) {
                        boolean loadFromNetwork = false;
                        if (e == null) {
                            if (toy == null) {
                                loadFromNetwork = true;
                            } else {
                                callback.done(toy, e);
                            }
                        } else {
                            loadFromNetwork = true;
                        }

                        if (loadFromNetwork) {
                            ParseQuery<Toy> q = new ParseQuery<Toy>(Toy.class);
                            q.whereEqualTo("objectId", objectId);
                            q.getFirstInBackground(new GetCallback<Toy>() {
                                @Override
                                public void done(Toy toy, ParseException e) {
                                    if (e == null)
                                        toy.pinInBackground();

                                    callback.done(toy, e);
                                }
                            });
                        }
                    }
                });
    }

    /**
     * @param fromServer 데이터를 불러오는 우선순위를 내부 저장소와 서버 중 무엇으로 기준을 잡을지 정하는 값.
     * */
    public void loadToys(final String ownerCode, final String categoryCode, final boolean fromServer, final FindCallback<Toy> callback) {
        if (!fromServer)
            fromLocalDatastore();

        if (!TextUtils.isEmpty(ownerCode))
            whereEqualTo("owner_code", ownerCode);

        whereEqualTo("category_code", categoryCode);

        orderByAscending("createdAt").findInBackground(new FindCallback<Toy>() {
            @Override
            public void done(List<Toy> toys, ParseException e) {
                if (fromServer) {
                    // from server..
                    if (e == null) {
                        for (Toy d : toys) {
                            d.pinInBackground();
                            WLog.i("pinning doll: " + d.getName());
                        }
                    }
                    callback.done(toys, e);

                } else {
                    // from local database..
                    if (toys == null || toys.isEmpty()) {
                        // try retrieving data from network..
                        ParseQuery<Toy> queryFromServer = new ParseQuery<Toy>(Toy.class);

                        if (!TextUtils.isEmpty(ownerCode))
                            queryFromServer.whereEqualTo("owner_code", ownerCode);

                        queryFromServer.whereEqualTo("category_code", categoryCode);
                        queryFromServer.orderByAscending("createdAt");

                        queryFromServer.findInBackground(new FindCallback<Toy>() {
                            @Override
                            public void done(List<Toy> toys, ParseException e) {
                                if (e == null)
                                    for (Toy d : toys) {
                                        d.pinInBackground();
                                        WLog.i("pinning doll: " + d.getName());
                                    }

                                callback.done(toys, e);
                            }
                        });
                    } else {
                        // retrieved from local database. callback immediately..
                        callback.done(toys, e);
                    }
                }
            }
        });
    }
}
