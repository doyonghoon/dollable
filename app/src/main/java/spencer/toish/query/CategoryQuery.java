package spencer.toish.query;

import android.support.annotation.NonNull;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

import spencer.toish.model.Category;

/**
 * Created by doyonghoon on 14. 12. 1..
 */
public class CategoryQuery extends ParseQuery<Category> {
    public CategoryQuery() {
        super(Category.class);
    }

    /**
     * 카테고리 목록을 background thread에서 불러옴.
     *
     * 유저의 카테고리 목록을 불러와야 하기 때문에 {userObjectId} 를 넣어주어야 함.
     * @param userObjectId 유저의 Id
     * @param callback 카테고리 목록을 콜백받을 인터페이스.
     * @param fromServer 서버에서 불러올땐 true.
     *
     * */
    public void load(@NonNull final String userObjectId, @NonNull final FindCallback callback, final boolean fromServer) {
        if (!fromServer)
            fromLocalDatastore();

        whereEqualTo("userCode", userObjectId);
        orderByAscending("createdAt").findInBackground(new FindCallback<Category>() {
            @Override
            public void done(List<Category> categories, ParseException e) {
                if (fromServer) {
                    // from server..
                    if (e == null) {
                        pinningInBackground(categories);
                    }
                    callback.done(categories, e);

                } else {
                    // from local database..
                    if (categories == null || categories.isEmpty()) {
                        // try retrieving data from network..
                        ParseQuery<Category> queryFromServer = new ParseQuery<>(Category.class);
                        queryFromServer.whereEqualTo("userCode", userObjectId);
                        queryFromServer.orderByAscending("createdAt");
                        queryFromServer.findInBackground(new FindCallback<Category>() {
                            @Override
                            public void done(List<Category> dolls, ParseException e) {
                                if (e == null)
                                    pinningInBackground(dolls);

                                callback.done(dolls, e);
                            }
                        });
                    } else {
                        // retrieved from local database. callback immediately..
                        callback.done(categories, e);
                    }
                }
            }
        });
    }

    private void pinningInBackground(List<Category> categories) {
        for (Category d : categories) {
            d.pinInBackground();
        }
    }
}
