package spencer.toish.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * 사용자가 모을 카테고리.
 * 카테고리 갯수로 화면의 탭 갯수가 정해짐.
 */
@ParseClassName("Category")
public class Category extends ParseObject {

    public static final String CATEGORY_CODE = "category_code";

    public void setUserCode(String userCode) {
        put("userCode", userCode);
    }

    public String getUserCode() {
        return getString("userCode");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getName() {
        return getString("name");
    }
}
