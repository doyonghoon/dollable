package spencer.toish.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import spencer.toish.util.BitmapUtil;

/**
 * 토이
 */
@ParseClassName("Toy")
public class Toy extends ParseObject {

    public String getDate() {
        final String d = getString("day");
        if (TextUtils.isEmpty(d) || d.equals("//")) return "";
        return d;
    }

    public void setOwner() {
        if (ParseUser.getCurrentUser() != null) {
            put("owner_code", ParseUser.getCurrentUser().getObjectId());
        }
    }

    public void setCategoryCode(String code) {
        put("category_code", code);
    }

    public String getCategoryCode() {
        return getString("category_code");
    }

    public String getOwnerCode() {
        return getString("owner_code");
    }

    public void setDate(String day) {
        put("day", day);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public String getType() {
        return getString("type");
    }

    public void setType(String type) {
        put("type", type);
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getName() {
        return getString("name");
    }

    public String getManufacturer() {
        return getString("manufacturer");
    }

    public void setManufacturer(String manufacturer) {
        put("manufacturer", manufacturer);
    }

    public String getSkin() {
        return getString("skin");
    }

    public void setSkin(String skin) {
        put("skin", skin);
    }

    public void setProfileImage(String path) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inSampleSize = 2;
        Bitmap b = BitmapFactory.decodeFile(path, opt);
        b = BitmapUtil.scaleDownEfficiently(b);
        ParseFile file = new ParseFile("main_image", BitmapUtil.convertBitmapToBytes(b));
        put("main_image", file);
    }

    public ParseFile getProfileImageFile() {
        return getParseFile("main_image");
    }
}
