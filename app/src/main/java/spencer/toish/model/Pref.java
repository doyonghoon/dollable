package spencer.toish.model;

import android.content.Context;

/**
 * Created by doyonghoon on 14. 11. 16..
 */
public class Pref {

    private static final String TIP_EDIT_LINK = "toish:tip.edit.link";
    private static final String TIP_DELETE_EXTRA_IMAGE = "toish:tip.delete.extra.image";

    public static boolean shouldShowEditLinkTip(Context c) {
        return c.getSharedPreferences(TIP_EDIT_LINK, Context.MODE_PRIVATE).getBoolean(TIP_EDIT_LINK, true);
    }

    public static boolean setEditLinkTip(Context c, boolean value) {
        return c.getSharedPreferences(TIP_EDIT_LINK, Context.MODE_PRIVATE).edit().putBoolean(TIP_EDIT_LINK, value).commit();
    }

    public static boolean shouldShowDeleteExtraImage(Context c) {
        return c.getSharedPreferences(TIP_DELETE_EXTRA_IMAGE, Context.MODE_PRIVATE).getBoolean(TIP_DELETE_EXTRA_IMAGE, true);
    }

    public static boolean setDeleteExtraImageTip(Context c, boolean value) {
        return c.getSharedPreferences(TIP_DELETE_EXTRA_IMAGE, Context.MODE_PRIVATE).edit().putBoolean(TIP_DELETE_EXTRA_IMAGE, value).commit();
    }
}
