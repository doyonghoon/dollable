package spencer.toish.model;

/**
 * Created by doyonghoon on 14. 11. 18..
 */
public enum OwnerProfileMenu {
    CHANGE_NICKNAME("닉네임 바꾸기"),
    CHANGE_PASSWORD("비밀번호 바꾸기"),
    RESET_DOLL("토이 모두 삭제하기"),
//    RESET_LINK("링크 모두 초기화 하기"),
    CHANGE_PROFILE_IMAGE("프로필 사진 바꾸기"),
    LOGOUT("로그아웃");

    private String mTitle;

    OwnerProfileMenu(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
