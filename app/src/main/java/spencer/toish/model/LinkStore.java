package spencer.toish.model;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

import spencer.toish.app.App;

/**
 * 링크 정보를 유저가 수정할 수 있도록 허용함.
 */
public class LinkStore {

    private boolean mAbleToLoadFromLocalDatabase = false;

    public interface LinkCallback {
        public void onSuccess(List<Link> links);
        public void onFail(ParseException e);
    }

    private static LinkStore sStore;

    public static LinkStore getInstance() {
        if (sStore == null) {
            sStore = new LinkStore();
        }
        return sStore;
    }

    public LinkStore() {
        List<Link> links = App.getDatabase().getAllLinks();
        mAbleToLoadFromLocalDatabase = !(links == null || links.isEmpty());
    }

    public void resetLinks(final LinkCallback callback) {
        fetchAllLinks(new LinkCallback() {
            @Override
            public void onSuccess(List<Link> links) {
                for (Link l : links) {
                    App.getDatabase().deleteLink(l);
                }
                createDummyLinks(callback);
            }

            @Override
            public void onFail(ParseException e) {
                callback.onFail(e);
            }
        });
    }

    private void createDummyLinks(final LinkCallback callback) {
        ParseQuery<Link> query = ParseQuery.getQuery(Link.class);
        query.findInBackground(new FindCallback<Link>() {
            @Override
            public void done(List<Link> links, ParseException e) {
                if (links != null) {
                    for (Link createdLink : links) {
                        App.getDatabase().addLink(createdLink);
                    }
                    mAbleToLoadFromLocalDatabase = true;
                    callback.onSuccess(links);
                } else {
                    mAbleToLoadFromLocalDatabase = false;
                    callback.onFail(e);
                }
            }
        });
    }

    public void fetchAllLinks(LinkCallback callback) {
        if (mAbleToLoadFromLocalDatabase) {
            callback.onSuccess(App.getDatabase().getAllLinks());
        } else {
            createDummyLinks(callback);
        }
    }
}
