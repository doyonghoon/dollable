package spencer.toish.model;

import android.support.v4.util.LruCache;

/**
 * Created by doyonghoon on 14. 11. 16..
 */
public class DollCache extends LruCache<String, Toy> {

    public DollCache() {
        super(5);
    }
}
