package spencer.toish.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by doyonghoon on 14. 11. 12..
 */
public class Database extends SQLiteOpenHelper {

    private Context mContext;
    private final static String LOCK = "dollable_database";
    private final static String NAME_DATABASE = "dollable.db";
    private final static int VERSION = 3;

    private static final String TABLE_DOLL = "DOLL";
    private static final String DOLL_ID = "doll_id";
    private static final String DOLL_CODE = "doll_code";
    private static final String DOLL_COVER_IMAGE = "doll_cover";
    private static final String DOLL_PROFILE_IMAGE = "doll_profile";
    private static final String DOLL_NAME = "doll_name";
    private static final String DOLL_MANUFACTURER = "doll_manufacturer";
    private static final String DOLL_SIZE = "doll_size";
    private static final String DOLL_TYPE = "doll_type_int";
    private static final String DOLL_DESCRIPTION = "doll_description";
    private static final String DOLL_SKIN = "doll_skin";
    private static final String DOLL_TIMESTAMP = "doll_timestamp";

    private static final String TABLE_LINK = "LINK";
    private static final String LINK_ID = "link_id";
    private static final String LINK_PATH = "link_path";
    private static final String LINK_TITLE = "link_name";
    private static final String LINK_TYPE = "link_type";

    private static final String TABLE_ADDITIONAL_IMAGE = "ADDITIONAL_IMAGE";
    private static final String ADDI_ID = "link_id";
    private static final String ADDI_PATH = "link_path";


    private final static String[] LINK_COLUMNS = {LINK_ID, LINK_TITLE, LINK_TYPE, LINK_PATH};
    private final static String[] ADDITIONAL_IMAGE_COLUMNS = {ADDI_ID, ADDI_PATH};
    private final static String[] DOLL_COLUMNS = {DOLL_ID, DOLL_CODE, DOLL_NAME, DOLL_PROFILE_IMAGE, DOLL_MANUFACTURER, DOLL_SIZE, DOLL_TYPE, DOLL_DESCRIPTION, DOLL_SKIN, DOLL_TIMESTAMP, DOLL_COVER_IMAGE};

    public Database(Context context) {
        super(context, NAME_DATABASE, null, VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_DOLL + "(_" + DOLL_ID + " text primary key," +
                DOLL_ID + " TEXT," +
                DOLL_CODE + " TEXT," +
                DOLL_NAME + " TEXT," +
                DOLL_PROFILE_IMAGE + " TEXT," +
                DOLL_MANUFACTURER + " TEXT," +
                DOLL_SIZE + " INTEGER," +
                DOLL_TYPE + " INTEGER," +
                DOLL_DESCRIPTION + " TEXT," +
                DOLL_SKIN + " TEXT," +
                DOLL_TIMESTAMP + " TEXT," +
                DOLL_COVER_IMAGE + " TEXT" +
                ");");

        db.execSQL("CREATE TABLE " + TABLE_LINK + "(_" + LINK_ID + " text primary key," +
                LINK_ID + " TEXT," +
                LINK_PATH + " TEXT," +
                LINK_TITLE + " TEXT," +
                LINK_TYPE + " INTEGER" +
                ");");

        db.execSQL("CREATE TABLE " + TABLE_ADDITIONAL_IMAGE + "(_" + ADDI_ID + " text primary key," +
                ADDI_ID + " TEXT," +
                ADDI_PATH + " TEXT" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOLL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDITIONAL_IMAGE);
        onCreate(db);
    }

    public void addLink(Link link) {
        synchronized (LOCK) {
            if (link != null) {
                SQLiteDatabase db = null;
                Cursor c = null;

                try {
                    db = getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(LINK_ID, link.getId());
                    values.put(LINK_TITLE, link.getTitle());
                    values.put(LINK_PATH, link.getLink());
                    values.put(LINK_TYPE, link.getType().getId());
                    c = db.query(TABLE_LINK, LINK_COLUMNS, LINK_ID + "=?", new String[]{link.getId()}, null, null, null, null);

                    if (c != null) {
                        c.moveToFirst();

                        if (c.getCount() == 0) {
//                            db.insert(TABLE_LINK, null, values);
                        } else {
//                            db.update(TABLE_LINK, values, LINK_ID + "=?", new String[]{LINK_ID});
                            db.delete(TABLE_LINK, LINK_ID + "=?", new String[]{link.getId()});
                        }
                        db.insert(TABLE_LINK, null, values);
                    }
                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (c != null) {
                        c.close();
                    }
                }
            }
        }
    }

    public void deleteLink(Link link) {
        synchronized (LOCK) {
            if (link != null) {
                SQLiteDatabase db = null;
                try {
                    db = getWritableDatabase();
                    db.delete(TABLE_LINK, LINK_ID + "=?", new String[]{link.getId()});
                } finally {
                    if (db != null)
                        db.close();
                }

            }
        }
    }

    public Link getLink(String id) {
        synchronized (LOCK) {
            if (!TextUtils.isEmpty(id)) {
                Link link = null;
                SQLiteDatabase db = null;
                Cursor cursor = null;
                try {
                    db = this.getReadableDatabase();

                    cursor = db.query(TABLE_LINK, LINK_COLUMNS, LINK_ID + "=?", new String[]{id}, null, null, null, null);

                    if (cursor != null) {
                        cursor.moveToFirst();

                        if (cursor.getCount() > 0) {
                            link = new Link();
                            link.setTitle(cursor.getString(cursor.getColumnIndex(LINK_TITLE)));
                            link.setLink(cursor.getString(cursor.getColumnIndex(LINK_PATH)));
                            link.setType(Link.LinkType.getLinkType(cursor.getInt(cursor.getColumnIndex(LINK_TYPE))));
                        }
                    }

                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (cursor != null) {
                        cursor.close();
                    }
                }

                return link;
            }
        }
        return null;
    }

    public List<Link> getAllLinks() {
        synchronized (LOCK) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            List<Link> links = new ArrayList<Link>();
            try {
                db = this.getReadableDatabase();
//                final String orderBy = LINK_TITLE + " Collate NOCASE";
                cursor = db.query(TABLE_LINK, LINK_COLUMNS, null, null, null, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    if (cursor.getCount() > 0) {
                        while (!cursor.isAfterLast()) {
                            Link link = new Link();
                            link.setTitle(cursor.getString(cursor.getColumnIndex(LINK_TITLE)));
                            link.setLink(cursor.getString(cursor.getColumnIndex(LINK_PATH)));
                            link.setType(Link.LinkType.getLinkType(cursor.getInt(cursor.getColumnIndex(LINK_TYPE))));
                            links.add(link);
                            cursor.moveToNext();
                        }
                    }
                }

            } finally {
                if (db != null) {
                    db.close();
                }

                if (cursor != null) {
                    cursor.close();
                }
            }
            return links;
        }
    }

    public void addDoll(Toy toy) {
        synchronized (LOCK) {
            if (toy != null) {
                SQLiteDatabase db = null;
                Cursor c = null;

                try {
                    db = getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(DOLL_ID, toy.getName());
                    values.put(DOLL_CODE, toy.getObjectId());
                    values.put(DOLL_DESCRIPTION, toy.getDescription());
                    values.put(DOLL_MANUFACTURER, toy.getManufacturer());
                    values.put(DOLL_NAME, toy.getName());
                    values.put(DOLL_TIMESTAMP, toy.getDate());
                    values.put(DOLL_SKIN, toy.getSkin());
                    values.put(DOLL_TYPE, toy.getType());

                    c = db.query(TABLE_DOLL, DOLL_COLUMNS, DOLL_ID + "=?", new String[]{toy.getName()}, null, null, null, null);

                    if (c != null) {
                        c.moveToFirst();

                        if (c.getCount() == 0) {
//                            db.insert(TABLE_DOLL, null, values);
                        } else {
                            db.delete(TABLE_DOLL, DOLL_ID + "=?", new String[]{toy.getName()});
                        }

                        db.insert(TABLE_DOLL, null, values);
                    }
                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (c != null) {
                        c.close();
                    }
                }
            }
        }
    }

    public void deleteDoll(Toy toy) {
        synchronized (LOCK) {
            if (toy != null) {
                SQLiteDatabase db = null;
                try {
                    db = getWritableDatabase();
                    db.delete(TABLE_DOLL, DOLL_ID + "=?", new String[]{toy.getName()});
                } finally {
                    if (db != null)
                        db.close();
                }

            }
        }
    }

    public Toy getDoll(String name) {
        synchronized (LOCK) {
            if (!TextUtils.isEmpty(name)) {
                Toy toy = null;
                SQLiteDatabase db = null;
                Cursor cursor = null;
                try {
                    db = this.getReadableDatabase();

                    cursor = db.query(TABLE_DOLL, DOLL_COLUMNS, DOLL_ID + "=?", new String[]{name}, null, null, null, null);

                    if (cursor != null) {
                        cursor.moveToFirst();

                        if (cursor.getCount() > 0) {
                            toy = new Toy();
                            toy.setObjectId(cursor.getString(cursor.getColumnIndex(DOLL_CODE)));
                            toy.setName(cursor.getString(cursor.getColumnIndex(DOLL_NAME)));
                            toy.setDescription(cursor.getString(cursor.getColumnIndex(DOLL_DESCRIPTION)));
                            toy.setManufacturer(cursor.getString(cursor.getColumnIndex(DOLL_MANUFACTURER)));
                            toy.setSkin(cursor.getString(cursor.getColumnIndex(DOLL_SKIN)));
                            toy.setDate(cursor.getString(cursor.getColumnIndex(DOLL_TIMESTAMP)));
                            toy.setType(cursor.getString(cursor.getColumnIndex(DOLL_TYPE)));
                        }
                    }

                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (cursor != null) {
                        cursor.close();
                    }
                }

                return toy;
            }
        }
        return null;
    }

    public List<Toy> getAllDolls() {
        synchronized (LOCK) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            List<Toy> toys = new ArrayList<Toy>();
            try {
                db = this.getReadableDatabase();
                final String orderBy = DOLL_SIZE + " DESC";
                cursor = db.query(TABLE_DOLL, DOLL_COLUMNS, null, null, null, null, orderBy, null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    if (cursor.getCount() > 0) {
                        while (!cursor.isAfterLast()) {
                            Toy toy = new Toy();
                            toy.setName(cursor.getString(cursor.getColumnIndex(DOLL_NAME)));
                            toy.setObjectId(cursor.getString(cursor.getColumnIndex(DOLL_CODE)));
                            toy.setDescription(cursor.getString(cursor.getColumnIndex(DOLL_DESCRIPTION)));
                            toy.setManufacturer(cursor.getString(cursor.getColumnIndex(DOLL_MANUFACTURER)));
                            toy.setSkin(cursor.getString(cursor.getColumnIndex(DOLL_SKIN)));
                            toy.setDate(cursor.getString(cursor.getColumnIndex(DOLL_TIMESTAMP)));
                            toy.setType(cursor.getString(cursor.getColumnIndex(DOLL_TYPE)));
                            toys.add(toy);
                            cursor.moveToNext();
                        }
                    }
                }

            } finally {
                if (db != null) {
                    db.close();
                }

                if (cursor != null) {
                    cursor.close();
                }
            }
            return toys;
        }
    }

    public List<ExtraImage> getAdditionalImages(String id) {
        synchronized (LOCK) {
            if (!TextUtils.isEmpty(id)) {
                List<ExtraImage> images = new ArrayList<ExtraImage>();
                SQLiteDatabase db = null;
                Cursor cursor = null;
                try {
                    db = this.getReadableDatabase();
                    cursor = db.query(TABLE_ADDITIONAL_IMAGE, ADDITIONAL_IMAGE_COLUMNS, ADDI_ID + "=?", new String[]{id}, null, null, null, null);

                    if (cursor != null) {
                        cursor.moveToFirst();

                        if (cursor.getCount() > 0) {
                            while (!cursor.isAfterLast()) {
                                ExtraImage extraImage = new ExtraImage();
                                extraImage.setCode(cursor.getString(cursor.getColumnIndex(ADDI_ID)));
                                extraImage.setImagePath(cursor.getString(cursor.getColumnIndex(ADDI_PATH)));
                                images.add(extraImage);
                                cursor.moveToNext();
                            }
                        }
                    }

                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (cursor != null) {
                        cursor.close();
                    }
                }

                return images;
            }
        }
        return null;
    }

    public void addAdditionalImage(ExtraImage image) {
        synchronized (LOCK) {
            if (image != null) {
                SQLiteDatabase db = null;
                Cursor c = null;

                try {
                    db = getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(ADDI_ID, image.getCode());
                    values.put(ADDI_PATH, image.getPath());

                    c = db.query(TABLE_ADDITIONAL_IMAGE, ADDITIONAL_IMAGE_COLUMNS, ADDI_PATH + "=?", new String[]{image.getPath()}, null, null, null, null);

                    if (c != null) {
                        c.moveToFirst();

                        if (c.getCount() == 0) {
//                            db.insert(TABLE_ADDITIONAL_IMAGE, null, values);
                        } else {
                            db.delete(TABLE_ADDITIONAL_IMAGE, ADDI_ID + "=?", new String[]{image.getCode()});
                        }
                        db.insert(TABLE_ADDITIONAL_IMAGE, null, values);
                    }
                } finally {
                    if (db != null) {
                        db.close();
                    }

                    if (c != null) {
                        c.close();
                    }
                }
            }
        }
    }

    public void deleteAdditionalImage(ExtraImage image) {
        synchronized (LOCK) {
            if (image != null) {
                SQLiteDatabase db = null;
                try {
                    db = getWritableDatabase();
                    db.delete(TABLE_ADDITIONAL_IMAGE, ADDI_PATH + "=?", new String[]{image.getPath()});
                } finally {
                    if (db != null)
                        db.close();

                    File f = new File(image.getPath());
                    f.delete();
                }
            }
        }
    }

    public void deleteAllTables() {
        synchronized (LOCK) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_DOLL, null, null);
            db.delete(TABLE_LINK, null, null);
            db.delete(TABLE_ADDITIONAL_IMAGE, null, null);
            db.close();
        }
    }

}
