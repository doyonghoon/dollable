package spencer.toish.model;

import android.text.TextUtils;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * 즐겨찾기 사이트
 */
@ParseClassName("Link")
public class Link extends ParseObject {

    public String getId() {
        return getString("address");
    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title))
            put("title", title);
    }

    public String getLink() {
        return getId();
    }

    public void setLink(String link) {
        if (!TextUtils.isEmpty(link))
            put("address", link);
    }

    public void setType(LinkType type) {
        if (type != null)
            put("type", type.getId());
    }

    public LinkType getType() {
        return LinkType.getLinkType(getInt("type"));
    }

    public boolean containsName(CharSequence s) {
        final String t = getTitle();
        return !TextUtils.isEmpty(t) && t.toLowerCase().contains(s);
    }

    public enum LinkType {
        DOLL(0x00, "Doll"),
        HAIR(0x01, "Hair"),
        CLOTHES(0x02, "Clothes");

        private String title;
        private int index;

        LinkType(int value, String title) {
            index = value;
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public int getId() {
            return index;
        }

        public static LinkType getLinkType(int index) {
            switch (index) {
                case 0x00:
                    return LinkType.DOLL;
                case 0x01:
                    return LinkType.HAIR;
                case 0x02:
                    return LinkType.CLOTHES;
                default:
                    return null;
            }
        }
    }
}
