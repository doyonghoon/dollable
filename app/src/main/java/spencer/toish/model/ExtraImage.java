package spencer.toish.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

import spencer.toish.util.BitmapUtil;

/**
 * Created by doyonghoon on 14. 10. 1..
 */
@ParseClassName("ExtraImage")
public class ExtraImage extends ParseObject {

    public ParseFile getImageFile() {
        return getParseFile("image_file");
    }

    public String getPath() {
        return getImageFile().getUrl();
    }

    public void setImageFile(ParseFile f) {
        if (f != null)
            put("image_file", f);
    }

    public void setImagePath(String path) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inSampleSize = 2;
        Bitmap b = BitmapFactory.decodeFile(path, opt);
        b = BitmapUtil.scaleDownEfficiently(b);
        ParseFile file = new ParseFile("image_file", BitmapUtil.convertBitmapToBytes(b));
        put("image_file", file);
    }

    public void setCode(String code) {
        put("code", code);
    }

    public String getCode() {
        return getString("code");
    }
}
