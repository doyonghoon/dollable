package spencer.toish.model;

import android.util.LruCache;

/**
 * Created by doyonghoon on 14. 11. 18..
 */
public class AlphaCache extends LruCache<String, Float> {
    public AlphaCache() {
        super(1);
    }
}
