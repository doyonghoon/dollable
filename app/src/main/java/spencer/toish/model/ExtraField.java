package spencer.toish.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by doyonghoon on 14. 11. 21..
 */
@ParseClassName("ExtraField")
public class ExtraField extends ParseObject {

    public void setCode(String objectId) {
        put("code", objectId);
    }

    public String getCode() {
        return getString("code");
    }

    public void setTitle(String title) {
        put("title", title);
    }

    public String getTitle() {
        return getString("title");
    }

    public void setValue(String value) {
        put("value", value);
    }

    public String getValue() {
        return getString("value");
    }
}
