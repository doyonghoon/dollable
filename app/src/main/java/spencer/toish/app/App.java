package spencer.toish.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.flurry.android.FlurryAgent;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.PushService;

import spencer.toish.model.AlphaCache;
import spencer.toish.model.Category;
import spencer.toish.model.Database;
import spencer.toish.model.Toy;
import spencer.toish.model.DollCache;
import spencer.toish.model.ExtraField;
import spencer.toish.model.ExtraImage;
import spencer.toish.model.Link;
import spencer.toish.ui.activity.SplashActivity;
import spencer.toish.ui.activity.TransparencyActivity;
import spencer.toish.ui.activity.UpActivity;
import spencer.toish.ui.fragment.BaseFragment;


/**
 * Created by doyonghoon on 14. 8. 10..
 */
public class App extends Application {

    private static App singleton;
    private Database mDatabase;
    private DollCache mDollCache;
    private AlphaCache mAlphaCache;

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        ParseObject.registerSubclass(Category.class);
        ParseObject.registerSubclass(Link.class);
        ParseObject.registerSubclass(Toy.class);
        ParseObject.registerSubclass(ExtraField.class);
        ParseObject.registerSubclass(ExtraImage.class);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "vctCVc0BtKZdvRyaARqfaDEsRBc9C52o4gUwmY5H", "hlpjnoBICkfDckscgey8ejiMe4DSosAMpbO5nmot");
        PushService.setDefaultPushCallback(this, SplashActivity.class);
    }

    public static App getInstance() {
        return singleton;
    }

    public static AlphaCache getAlphaCache() {
        if (getInstance().mAlphaCache == null) {
            getInstance().mAlphaCache = new AlphaCache();
            getInstance().mAlphaCache.put("alpha", 0.0f);
        }
        return getInstance().mAlphaCache;
    }

    public static Database getDatabase() {
        if (getInstance().mDatabase == null)
            getInstance().mDatabase = new Database(getInstance());
        return getInstance().mDatabase;
    }

    public static DollCache getDollCache() {
        if (getInstance().mDollCache == null) {
            getInstance().mDollCache = new DollCache();
        }
        return getInstance().mDollCache;
    }

    public static void goTo(@NonNull Context context, @NonNull BaseFragment.FragmentType type, boolean showDrawer, @Nullable Bundle b) {
        goTo(context, type, b, showDrawer, 0);
    }

    public static void goTo(Context context, BaseFragment.FragmentType type, Bundle b, boolean showDrawer, int requestCode) {
        goTo(context, type, b, showDrawer, requestCode, false);
    }

    public static void goTo(Context context, BaseFragment.FragmentType type, Bundle b, boolean showDrawer, int requestCode, boolean enableTransparencyToolbar) {
        if (context instanceof Activity) {
            if (b == null)
                b = new Bundle();

            Activity act = (Activity) context;
            Intent i = new Intent(act, enableTransparencyToolbar ? TransparencyActivity.class : UpActivity.class);
            b.putInt(BaseFragment.ARG_BASE_NUMBER, type.getArg());
            b.putBoolean("showDrawer", showDrawer);

            i.putExtras(b);
            ((Activity) context).startActivityForResult(i, requestCode);
        }
    }
}
