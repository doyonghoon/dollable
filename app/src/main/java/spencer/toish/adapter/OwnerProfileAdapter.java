package spencer.toish.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.model.OwnerProfileMenu;

/**
 * Created by doyonghoon on 14. 11. 18..
 */
public class OwnerProfileAdapter extends RecyclerView.Adapter<OwnerProfileAdapter.ViewHolder> {

    private Context mContext;
    private OnProfileItemClickListener mListener;
    private List<OwnerProfileMenu> mMenus = new ArrayList<OwnerProfileMenu>();

    public OwnerProfileAdapter(Context context) {
        mContext = context;
        mMenus.add(OwnerProfileMenu.CHANGE_PROFILE_IMAGE);
        mMenus.add(OwnerProfileMenu.CHANGE_NICKNAME);
        mMenus.add(OwnerProfileMenu.CHANGE_PASSWORD);
        mMenus.add(OwnerProfileMenu.RESET_DOLL);
//        mMenus.add(OwnerProfileMenu.RESET_LINK);
        mMenus.add(OwnerProfileMenu.LOGOUT);
    }

    public void setOnClickListener(OnProfileItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item_profile_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        vh.setItemClickListener(mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final OwnerProfileMenu item = mMenus.get(i);
        viewHolder.setItem(item);
    }

    @Override
    public int getItemCount() {
        return mMenus.size();
    }

    public interface OnProfileItemClickListener {
        public void onClick(OwnerProfileMenu menu);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.title) TextView title;
        private OwnerProfileMenu mMenu;
        private OnProfileItemClickListener mListener;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null)
                mListener.onClick(mMenu);
        }

        public void setItemClickListener(OnProfileItemClickListener listener) {
            mListener = listener;
        }

        public void setItem(OwnerProfileMenu menu) {
            mMenu = menu;
            title.setText(mMenu.getTitle());
        }
    }
}
