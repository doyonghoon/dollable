package spencer.toish.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.model.ExtraImage;
import spencer.toish.model.Pref;
import spencer.toish.ui.activity.BaseActivity;
import spencer.toish.ui.fragment.BaseFragment;
import spencer.toish.ui.fragment.DollDetailFragment;
import spencer.toish.ui.view.widget.tip.ShowTipsBuilder;
import spencer.toish.ui.view.widget.tip.ShowTipsView;
import spencer.toish.ui.view.widget.tip.ShowTipsViewInterface;

/**
 * Created by doyonghoon on 14. 11. 12..
 */
public class ExtraImageAdapter extends ArrayAdapter<ExtraImage> {

    private boolean mShouldShowTip = false;

    public ExtraImageAdapter(Context context, boolean shouldShowTip) {
        super(context, R.layout.list_item_extra_image);
        mShouldShowTip = shouldShowTip;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_extra_image, parent, false);
            holder = new ViewHolder((ImageView) convertView, position, getItem(position));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ExtraImage image = getItem(position);
        Picasso.with(getContext())
                .load(image.getImageFile().getUrl())
                .placeholder(new ColorDrawable(getContext().getResources().getColor(R.color.background_floating_material_dark)))
                .resizeDimen(R.dimen.extra_rectangle_image_size, R.dimen.extra_rectangle_image_size)
                .centerCrop()
                .into(holder.imageView);

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                switch (position) {
                    case 0:
                        Toast.makeText(getContext(), "메인 사진은 삭제할 수 없습니다", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        showRemoveDialog(holder.getImage());
                        break;
                }
                return true;
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> paths = new ArrayList<String>();
                for (int i = 0; i < getCount(); i++) {
                    ExtraImage image = getItem(i);
                    paths.add(image.getPath());
                }
                Bundle b = new Bundle();
                b.putInt("current_item", holder.getPosition());
                b.putStringArrayList("paths", paths);
                App.goTo(getContext(), BaseFragment.FragmentType.DOLL_IMAGE_GALLERY, false, b);
            }
        });

        if (mShouldShowTip && getCount() > 1 && position == 1) {
            mShouldShowTip = false;
            ShowTipsView showtips = new ShowTipsBuilder((Activity) getContext())
                    .setTarget(convertView)
                    .setTitle("사진 삭제하기")
                    .setDescription("링크를 길게 누르고 있으면 삭제를 할 수 있어요")
                    .setDelay(300)
                    .setCircleColor(getContext().getResources().getColor(R.color.accent_pressed))
                    .setCallback(new ShowTipsViewInterface() {
                        @Override
                        public void gotItClicked() {
                            Pref.setDeleteExtraImageTip(getContext(), false);
                        }
                    })
                    .build();

            showtips.show((Activity) getContext());
        }
        return convertView;
    }

    private MaterialDialog mDialog = null;
    private void showRemoveDialog(final ExtraImage image) {
        mDialog = new MaterialDialog(getContext());
        mDialog.setTitle("삭제하기");
        mDialog.setMessage("정말 삭제하시겠어요?");
        mDialog.setPositiveButton("삭제", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String imageCode = image.getCode();
                image.unpinInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            image.deleteInBackground(new DeleteCallback() {
                                @Override
                                public void done(ParseException e) {
                                    BaseActivity act = (BaseActivity) getContext();
                                    DollDetailFragment frag = (DollDetailFragment) act.getFragment();
                                    frag.updateUi(imageCode, null);
                                }
                            });
                        } else {
                            Toast.makeText(getContext(), "사진을 삭제하지 못했습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                mDialog.dismiss();
            }
        });

        mDialog.setNegativeButton("아니요", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    static class ViewHolder {
        @InjectView(R.id.extra_image)
        ParseImageView imageView;

        private ExtraImage image;
        private int position;

        ViewHolder(ImageView v, int position, ExtraImage image) {
            this.position = position;
            ButterKnife.inject(this, v);
            this.image = image;
        }

        public ExtraImage getImage() {
            return image;
        }

        public int getPosition() {
            return position;
        }
    }
}
