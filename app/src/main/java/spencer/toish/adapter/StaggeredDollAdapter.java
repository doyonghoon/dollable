package spencer.toish.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by doyonghoon on 14. 11. 8..
 */
public class StaggeredDollAdapter extends RecyclerView.Adapter<StaggeredDollAdapter.ViewHolder> {

    private Context mContext;
    private String[] mDataSet = null;

    public StaggeredDollAdapter(Context context) {
        mContext = context;
    }

    public void setData(String[] dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        TextView v = new TextView(mContext);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.mTextView.setText(mDataSet[i]);
    }

    @Override
    public int getItemCount() {
        return mDataSet == null || mDataSet.length <= 0 ? 0 : mDataSet.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ViewHolder(TextView v) {
            super(v);
            mTextView = v;
        }
    }
}
