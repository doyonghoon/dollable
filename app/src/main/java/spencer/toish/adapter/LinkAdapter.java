package spencer.toish.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import dreamers.graphics.RippleDrawable;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import spencer.toish.R;
import spencer.toish.model.Link;
import spencer.toish.model.Pref;
import spencer.toish.ui.fragment.LinkEditDialogFragment;
import spencer.toish.ui.view.widget.tip.ShowTipsBuilder;
import spencer.toish.ui.view.widget.tip.ShowTipsView;
import spencer.toish.ui.view.widget.tip.ShowTipsViewInterface;

/**
 * 링크 리스트 아이템
 */
public class LinkAdapter extends ArrayAdapter<Link> implements StickyListHeadersAdapter {

    private FragmentManager mFragmentManager;
    private boolean mShouldShowEditLinkTip = false;

    public LinkAdapter(Context context, FragmentManager manager, boolean shouldShowEditLinkTip) {
        super(context, R.layout.list_item_link);
        mFragmentManager = manager;
        mShouldShowEditLinkTip = shouldShowEditLinkTip;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinkViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_link, null);
            RippleDrawable.createRipple(convertView, getContext().getResources().getColor(R.color.material_blue_600));
            holder = new LinkViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (LinkViewHolder) convertView.getTag();
        }

        final Link l = getItem(position);
        holder.title.setText(l.getTitle());
        holder.link.setText(l.getLink());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (URLUtil.isValidUrl(l.getLink())) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(l.getLink()));
                    getContext().startActivity(i);
                } else {
                    Toast.makeText(getContext(), "링크가 이상해요!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                LinkEditDialogFragment dialogFragment = new LinkEditDialogFragment();
                Bundle b = new Bundle();
                b.putString("title", l.getTitle());
                b.putString("path", l.getLink());
                b.putInt("type", l.getType().getId());
                dialogFragment.setArguments(b);
//                dialogFragment.show(mFragmentManager, "link_edit");
                return true;
            }
        });

        if (mShouldShowEditLinkTip) {
            mShouldShowEditLinkTip = false;
            ShowTipsView showtips = new ShowTipsBuilder((Activity) getContext())
                    .setTarget(convertView)
                    .setTitle("링크 수정하기")
                    .setDescription("링크를 길게 누르고 있으면 수정이나 삭제를 할 수 있어요")
                    .setDelay(1000)
                    .setCircleColor(getContext().getResources().getColor(R.color.accent_pressed))
                    .setCallback(new ShowTipsViewInterface() {
                        @Override
                        public void gotItClicked() {
                            Pref.setEditLinkTip(getContext(), false);
                        }
                    })
                    .build();

            showtips.show((Activity) getContext());
        }

        return convertView;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        HeaderHolder holder;

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_item_header_text, null);
            holder = new HeaderHolder(view);
            view.setTag(holder);
        } else {
            holder = (HeaderHolder) view.getTag();
        }

        holder.text.setText(getItem(i).getType().getTitle());

        return view;
    }

    @Override
    public long getHeaderId(int i) {
        return getItem(i).getType().hashCode();
    }

    static class LinkViewHolder {
        @InjectView(R.id.link_title)
        TextView title;
        @InjectView(R.id.link)
        TextView link;

        LinkViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    static class HeaderHolder {
        @InjectView(R.id.header_text) TextView text;

        HeaderHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
