package spencer.toish.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.model.Toy;
import spencer.toish.util.BitmapUtil;
import spencer.toish.util.DdayUtil;

/**
 * 토이 카드
 */
public class DollAdapter extends ArrayAdapter<Toy> {

    public DollAdapter(Context context) {
        super(context, R.layout.cell_doll);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cell_doll, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Toy toy = getItem(position);
        holder.title.setText(toy.getName());

        if (!TextUtils.isEmpty(toy.getDate())) {
            holder.subtitle.setText(DdayUtil.calculateDate(toy.getDate()));
            holder.subtitle.setVisibility(View.VISIBLE);
        } else {
            holder.subtitle.setText(null);
            holder.subtitle.setVisibility(View.GONE);
        }

        holder.image.setImageBitmap(null);
        setLayoutParams(holder.contentLayout, (holder.title.getVisibility() == View.VISIBLE && holder.subtitle.getVisibility() == View.VISIBLE));

        if (toy.getProfileImageFile() != null) {
            Picasso.with(getContext())
                    .load(toy.getProfileImageFile().getUrl())
                    .fit()
                    .placeholder(new ColorDrawable(getContext().getResources().getColor(R.color.background_floating_material_dark)))
                    .into(holder.image, new Callback() {
                        @Override
                        public void onSuccess() {
                            setDynamicColor(BitmapUtil.drawableToBitmap(holder.image.getDrawable()), holder.contentLayout, holder.title, holder.subtitle);
                        }

                        @Override
                        public void onError() {
                            holder.image.setImageBitmap(null);
                        }
                    });
        } else {
            holder.image.setImageBitmap(null);
        }

        return convertView;
    }

    private void setDynamicColor(Bitmap bitmap, final RelativeLayout layout, final TextView title, final TextView subtitle) {
        layout.setBackgroundColor(getContext().getResources().getColor(R.color.theme_primary));
        title.setTextColor(Color.WHITE);
        subtitle.setTextColor(Color.WHITE);

        Palette.generateAsync(bitmap,
                new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {
                        Palette.Swatch swatch = palette.getVibrantSwatch();
                        if (swatch != null) {
                            swatch.getBodyTextColor();
                            layout.setBackgroundColor(swatch.getRgb());
                            title.setTextColor(swatch.getTitleTextColor());
                            subtitle.setTextColor(swatch.getBodyTextColor());
                        }
                    }
                });
    }

    private void setLayoutParams(View v, boolean isDoubleLine) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
        lp.height = getContext().getResources().getDimensionPixelSize(isDoubleLine ? R.dimen.cell_two_line : R.dimen.cell_single_line);
        v.setLayoutParams(lp);
    }

    static class ViewHolder {
        @InjectView(R.id.doll_image) ParseImageView image;
        @InjectView(R.id.doll_title) TextView title;
        @InjectView(R.id.doll_subtitle) TextView subtitle;
        @InjectView(R.id.doll_content_layout) RelativeLayout contentLayout;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
