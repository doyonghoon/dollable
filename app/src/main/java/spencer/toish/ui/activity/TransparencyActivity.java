package spencer.toish.ui.activity;

import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;

import spencer.toish.R;
import spencer.toish.app.App;

/**
 * Created by doyonghoon on 14. 11. 13..
 */
public class TransparencyActivity extends UpActivity {

    private ValueAnimator mAlphaAnimator = null;
    private ValueAnimator mHideAnimator = null;
    private boolean isRunningVisibleAnimator, isRunningHideAnimator = false;

    protected int getLayoutId() {
        return R.layout.act_transparency;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        d = getResources().getDrawable(R.color.theme_primary);
        setToolbarTransparent();
    }

    public void setToolbarTransparent() {
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setBackgroundDrawable(d);
        d.setAlpha(0);
    }

    public void animateVisibleToolbar() {
        if (isRunningVisibleAnimator) return;
        if (mAlphaAnimator == null) {
            mAlphaAnimator = new ValueAnimator().setDuration(250);
            mAlphaAnimator.setFloatValues(0.0f, 1.0f);
            mAlphaAnimator.setInterpolator(new AccelerateInterpolator());
            mAlphaAnimator.addUpdateListener(getVisibleAnimator());
        }

        if (!mAlphaAnimator.isRunning())
            mAlphaAnimator.start();
    }

    public void animateHideToolbar() {
        if (isRunningHideAnimator) return;
        if (mHideAnimator == null) {
            mHideAnimator = new ValueAnimator().setDuration(250);
            mHideAnimator.setFloatValues(1.0f, 0.0f);
            mHideAnimator.setInterpolator(new AccelerateInterpolator());
            mHideAnimator.addUpdateListener(getHideAnimator());
        }

        if (!mHideAnimator.isRunning())
            mHideAnimator.start();
    }

    private ValueAnimator.AnimatorUpdateListener getHideAnimator() {
        return new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float alpha = Float.valueOf(String.valueOf(animation.getAnimatedValue()));
                App.getAlphaCache().put("alpha", alpha);
                d.setAlpha((int) (alpha * 255));

                if (alpha >= 0.9f)
                    isRunningHideAnimator = false;
            }
        };
    }

    private Drawable d = null;

    private ValueAnimator.AnimatorUpdateListener getVisibleAnimator() {
        return new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float alpha = Float.valueOf(String.valueOf(animation.getAnimatedValue()));
                App.getAlphaCache().put("alpha", alpha);
                d.setAlpha((int) (alpha * 255));

                if (alpha <= 0.1f)
                    isRunningVisibleAnimator = false;
            }
        };
    }
}
