package spencer.toish.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;

import spencer.toish.ui.fragment.BaseFragment;
import spencer.toish.util.WLog;

/**
 * 위로가는 버튼
 */
public class UpActivity extends BaseActivity {

    @Override
    protected boolean needDrawerFragment() {
        return getBooleanFromBundle("showDrawer", false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getBooleanFromBundle("canGoBack", true)) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(needDrawerFragment() || getBooleanFromBundle("canGoBack", true));
            actionbar.setDisplayShowHomeEnabled(true);
        }

        replaceFragment(BaseFragment.getTypeByArg(getBaseFragmentArgumentInt(getIntent())));
    }

    protected boolean getBooleanFromBundle(String key, boolean defaultValue) {
        if (getIntent() != null && getIntent().hasExtra(key)) {
            return getIntent().getBooleanExtra(key, defaultValue);
        }
        return defaultValue;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch (position) {
            case 0:
//                App.goTo(this, BaseFragment.FragmentType.CATEGORY_LIST, false, null);
                break;
        }
    }
}
