package spencer.toish.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.ui.fragment.BaseFragment;

/**
 * Created by doyonghoon on 14. 11. 16..
 */
public class LoginActivity extends ActionBarActivity {

    @InjectView(R.id.email) MaterialEditText mEmail;
    @InjectView(R.id.password) MaterialEditText mPassword;
    @InjectView(R.id.nickname) MaterialEditText mNickname;
    @InjectView(R.id.login) Button mLoginButton;
    @InjectView(R.id.change_mode) Button mChangeMode;

    private boolean mLoginMode = true;
    private boolean isAnimating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);
        ButterKnife.inject(this);

        updateUi();
    }

    private void updateUi() {
        mEmail.setText(null);
        mPassword.setText(null);
        mNickname.setText(null);
        mEmail.clearFocus();
        mPassword.clearFocus();
        mNickname.clearFocus();

        if (mLoginMode) {
            mNickname.setVisibility(View.GONE);
            mLoginButton.setText("로그인");
            mChangeMode.setText("회원가입 하기");
            mNickname.setVisibility(View.GONE);
        } else {
            mNickname.setVisibility(View.VISIBLE);
            mLoginButton.setText("회원가입");
            mChangeMode.setText("로그인 하기");
            mNickname.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.change_mode)
    public void onClickChangeMode() {
        mLoginMode = !mLoginMode;
        updateUi();
    }

    @OnClick(R.id.login)
    public void onClickLogin() {
        if (mLoginMode) {
            if (TextUtils.isEmpty(mEmail.getText().toString()) || !isEmail(mEmail.getText().toString())) {
                showAlert("이메일 형식이 아닙니다. 다시 확인해주세요");
                return;
            }

            if (TextUtils.isEmpty(mPassword.getText().toString())) {
                showAlert("비밀번호가 비어있습니다. 최소 4자 이상은 적어야 안전해요");
                return;
            }

            login(mEmail.getText().toString(), mPassword.getText().toString());

        } else {
            if (TextUtils.isEmpty(mEmail.getText().toString()) || !isEmail(mEmail.getText().toString())) {
                showAlert("이메일 형식이 아닙니다. 다시 확인해주세요");
                return;
            }

            if (TextUtils.isEmpty(mNickname.getText().toString())) {
                showAlert("닉네임이 비어있습니다. 다른 유저와 교류하기 위해선 반드시 필요한 정보입니다");
                return;
            }

            if (TextUtils.isEmpty(mPassword.getText().toString())) {
                showAlert("비밀번호가 비어있습니다. 최소 4자 이상은 적어야 안전해요");
                return;
            }

            signup(mEmail.getText().toString(), mPassword.getText().toString(), mNickname.getText().toString());
        }
    }

    private void signup(String email, String password, String nickname) {
        showProgressDialog();
        ParseUser user = new ParseUser();
        user.setUsername(email);
        user.setEmail(email);
        user.setPassword(password);
        user.put("nickname", nickname);
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                dismissProgressDialog();
                if (e == null) {
                    goToMain();
                } else {
                    showAlert("회원가입에 실패했습니다");
                }
            }
        });
    }

    private void login(String email, String password) {
        showProgressDialog();
        ParseUser.logInInBackground(email, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                dismissProgressDialog();
                if (user != null) {
                    goToMain();
                } else {
                    showAlert("로그인 과정이 뭔가 잘못됐어요. 이메일과 비밀번호를 다시 한 번 확인해주세요");
                }
            }
        });
    }

    private ProgressDialog mDialog = null;

    private void showProgressDialog() {
        if (mDialog == null) {
            mDialog = new ProgressDialog(this);
            mDialog.setCancelable(false);
        }
        mDialog.setMessage(mLoginMode ? "로그인 중.." : "회원가입 중..");
        mDialog.show();
    }

    private void dismissProgressDialog() {
        if (mDialog != null)
            mDialog.dismiss();
    }

    private void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void goToMain() {
        Bundle b = new Bundle();
        b.putBoolean("canGoBack", false);
        App.goTo(LoginActivity.this, BaseFragment.FragmentType.CATEGORY_LIST, true, b);
        finish();
    }

    private boolean isEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword() {
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "Y9GRZ83GVKMKPP2D5YNH");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
