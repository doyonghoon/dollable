package spencer.toish.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.flurry.android.FlurryAgent;
import com.parse.ParseAnalytics;

import spencer.toish.R;
import spencer.toish.ui.fragment.BaseFragment;
import spencer.toish.ui.fragment.NavigationDrawerFragment;
import spencer.toish.util.WLog;

/**
 * Created by doyonghoon on 14. 9. 19..
 */
public abstract class BaseActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, DialogInterface.OnDismissListener {

    private String mCurrentFragmentTag = "";

    Toolbar mToolbar;
    private NavigationDrawerFragment mNavigationDrawerFragment = null;

    protected int getLayoutId() {
        return needDrawerFragment() ? R.layout.act_base : R.layout.act_up;
    }

    protected boolean needDrawerFragment() {
        return false;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        WLog.i("position: " + position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        mToolbar = (Toolbar) findViewById(R.id.toolbar_global);
        mToolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        if (needDrawerFragment()) {
            // Set up the drawer.
            mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
            if (mNavigationDrawerFragment != null)
                mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "Y9GRZ83GVKMKPP2D5YNH");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    public android.support.v7.widget.Toolbar getToolbar() {
        return mToolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        mToolbar = toolbar;
        setSupportActionBar(mToolbar);
    }

    public void replaceFragment(BaseFragment.FragmentType type) {
        Bundle b = getIntent() != null && getIntent().getExtras() != null ? getIntent().getExtras() : null;
        final BaseFragment frag = BaseFragment.newInstance(type, b);
        mCurrentFragmentTag = "Section " + (type.getScreenName());

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(mCurrentFragmentTag)
                .replace(R.id.container, frag, mCurrentFragmentTag)
                .commit();
    }

    protected int getBaseFragmentArgumentInt(Intent intent) {
        final int defaultResult = 0;
        if (intent == null) return defaultResult;
        if (intent.hasExtra(BaseFragment.ARG_BASE_NUMBER)) {
            return intent.getIntExtra(BaseFragment.ARG_BASE_NUMBER, defaultResult);
        }
        return defaultResult;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (getFragment() != null)
            getFragment().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        // close drawer if navigation drawer is opened..
        if (mNavigationDrawerFragment != null && mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.closeDrawer();
            return;
        }

        if (manager.getBackStackEntryCount() < 2) {
            if (getFragment().canBackPressed())
                finish();
        } else {
            if (manager.popBackStackImmediate()) {
                final String name = manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 1).getName();

                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(name);

            } else {
                if (getFragment().canBackPressed())
                    finish();
            }
        }
    }

    public BaseFragment getFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(mCurrentFragmentTag);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getFragment() != null && getFragment() instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) getFragment()).onDismiss(dialog);
        }
    }
}
