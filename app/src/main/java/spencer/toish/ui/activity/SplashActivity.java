package spencer.toish.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.flurry.android.FlurryAgent;
import com.parse.ParseUser;

import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.ui.fragment.BaseFragment;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash);
        if (getActionBar() != null)
            getActionBar().hide();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLoggedIn()) {
                    goToMain();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
                finish();
            }
        }, 100);
    }

    private void goToMain() {
        Bundle b = new Bundle();
        b.putBoolean("canGoBack", false);
        App.goTo(SplashActivity.this, BaseFragment.FragmentType.CATEGORY_LIST, true, b);
    }

    private boolean isLoggedIn() {
        return ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().isAuthenticated();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "Y9GRZ83GVKMKPP2D5YNH");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
