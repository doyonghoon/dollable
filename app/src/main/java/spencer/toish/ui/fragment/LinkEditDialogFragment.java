package spencer.toish.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;
import me.drakeet.materialdialog.MaterialDialog;
import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.model.Link;

/**
 * 사이트 목록의 사이트를 수정하는 다이얼로그
 */
public class LinkEditDialogFragment extends BlurDialogFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null && getActivity() instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) getActivity()).onDismiss(dialog);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View customView = getActivity().getLayoutInflater().inflate(R.layout.dialog_link_edit, null);
        Button submit = ButterKnife.findById(customView, R.id.submit);
        Button delete = ButterKnife.findById(customView, R.id.delete);
        final EditText link = ButterKnife.findById(customView, R.id.text_link);
        final EditText name = ButterKnife.findById(customView, R.id.text_name);
        final TextView type = ButterKnife.findById(customView, R.id.text_type);
        final TextView dialogTitle = ButterKnife.findById(customView, R.id.title);

        Bundle b = getArguments();
        final String linkText = b.getString("path");
        final String titleText = b.getString("title");
        final int typeInt = b.getInt("type");
        final Link linkData = new Link();
        if (!TextUtils.isEmpty(titleText))
            linkData.setTitle(titleText);
        if (!TextUtils.isEmpty(linkText))
            linkData.setLink(linkText);
        linkData.setType(Link.LinkType.getLinkType(typeInt));
        mLinkType = linkData.getType();

        final boolean isEdit = !TextUtils.isEmpty(linkText) && !TextUtils.isEmpty(titleText);

        if (isEdit) {
            dialogTitle.setText("수정하기");
            delete.setVisibility(View.VISIBLE);
        } else {
            dialogTitle.setText("추가하기");
            delete.setVisibility(View.INVISIBLE);
        }

        link.setText(linkText);
        name.setText(titleText);
        type.setText(linkData.getType().getTitle());

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTypeMenu == null) {
                    mTypeMenu = new PopupMenu(getActivity(), type);
                    mTypeMenu.getMenuInflater().inflate(R.menu.menu_link_type, mTypeMenu.getMenu());
                    mTypeMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            mLinkType = Link.LinkType.getLinkType(item.getOrder());
                            type.setText(item.getTitle());
                            return true;
                        }
                    });
                }

                mTypeMenu.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(link.getText().toString()) && !TextUtils.isEmpty(name.getText().toString())) {
                    Link modifiedLink = new Link();
                    modifiedLink.setLink(link.getText().toString());
                    modifiedLink.setTitle(name.getText().toString());
                    modifiedLink.setType(mLinkType);
                    if (isEdit) {
                        App.getDatabase().deleteLink(linkData);
                    }
                    App.getDatabase().addLink(modifiedLink);
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "이름과 링크를 입력해주세요", Toast.LENGTH_SHORT).show();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(linkData);
            }
        });

        builder.setView(customView);
        builder.setTitle(null);
        return builder.create();
    }

    private MaterialDialog mDialog;

    private void showDeleteDialog(final Link linkData) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mDialog = new MaterialDialog(getActivity());
        mDialog.setTitle("삭제하기");
        mDialog.setMessage("정말 삭제하시겠어요?");
        mDialog.setPositiveButton("삭제", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getDatabase().deleteLink(linkData);
                Toast.makeText(getActivity(), linkData.getTitle() + " 사이트가 제거되었습니다!", Toast.LENGTH_SHORT).show();
                mDialog.dismiss();
                dismiss();
            }
        });

        mDialog.setNegativeButton("아니요", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private Link.LinkType mLinkType = Link.LinkType.DOLL;
    private PopupMenu mTypeMenu;
}
