package spencer.toish.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.model.Category;
import spencer.toish.query.CategoryQuery;
import spencer.toish.ui.view.widget.ChangeFieldView;
import spencer.toish.ui.view.widget.SlidingTabLayout;
import spencer.toish.util.WLog;

/**
 * Created by doyonghoon on 14. 11. 30..
 */
public class CategoryFragment extends BaseFragment {

    @InjectView(R.id.tabs) SlidingTabLayout mTabs;
    @InjectView(R.id.view_pager) ViewPager mViewPager;
    @InjectView(R.id.empty_view) ViewGroup mEmptyView;

    private OurViewPagerAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_category;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        final String userId = ParseUser.getCurrentUser() != null ? ParseUser.getCurrentUser().getObjectId() : null;
        if (!TextUtils.isEmpty(userId))
            FlurryAgent.setUserId(userId);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        update(ParseUser.getCurrentUser().getObjectId(), 0, false);
    }

    private void update(String userCode, final int position, boolean fromServer) {
        CategoryQuery q = new CategoryQuery();
        q.load(userCode, new FindCallback() {
            @Override
            public void done(List categories, ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "카테고리를 불러오지 못했습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                } else {
                    if (categories == null || categories.isEmpty()) {
                        mEmptyView.setVisibility(View.VISIBLE);
                    } else {
                        mEmptyView.setVisibility(View.GONE);
                        mAdapter = new OurViewPagerAdapter(getFragmentManager());
                        mAdapter.setCategories(categories);
                        mViewPager.setAdapter(mAdapter);
                        mTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);

                        mTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
                        mTabs.setDistributeEvenly(true);
                        mTabs.setViewPager(mViewPager);
                        mViewPager.setCurrentItem(position);
                    }
                }
            }
        }, fromServer);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int position = 0;
            if (data != null) {
                final String callbackCategoryCode = data.getStringExtra(Category.CATEGORY_CODE);
                if (!TextUtils.isEmpty(callbackCategoryCode)) {
                    List<Category> categories = mAdapter.getCategories();
                    for (Category c : categories) {
                        if (callbackCategoryCode.equals(c.getObjectId())) {
                            break;
                        }
                        position++;
                    }
                }
            }
            update(ParseUser.getCurrentUser().getObjectId(), position, true);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_doll_list, menu);
        MenuItem add = menu.findItem(R.id.action_add);
        add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        WLog.i("menuView: " + (add.getActionView() == null ? "null" : "not null.."));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_add:
                showAddCategoryDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("새로운 카테고리 추가하기");

        final ChangeFieldView v = new ChangeFieldView(getActivity());
        builder.setView(v);
        v.setHint("건담");
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                final ProgressDialog d = new ProgressDialog(getActivity());
                d.setMessage(v.getValue() + " 추가중..");
                d.setCancelable(false);
                d.show();
                final Category c = new Category();
                final String userCode = ParseUser.getCurrentUser().getObjectId();
                c.setUserCode(userCode);
                c.setName(v.getValue());
                c.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        d.dismiss();
                        String message = null;
                        if (e == null) {
                            message = v.getValue() + " 카테고리가 추가되었습니다!";
                            update(userCode, 0, true);
                        } else {
                            message = "카테고리를 추가하지 못했습니다. 다시 시도해주세요";
                        }
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private class OurViewPagerAdapter extends FragmentPagerAdapter {

        private List<Category> mCategories;

        public OurViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mCategories = new ArrayList<Category>();
        }

        public void setCategories(List<Category> categories) {
            mCategories = categories;
        }

        public List<Category> getCategories() {
            return mCategories;
        }

        @Override
        public Fragment getItem(int position) {
            DollListFragment frag = new DollListFragment();
            Bundle args = new Bundle();
            args.putString(Category.CATEGORY_CODE, mCategories.get(position).getObjectId());
            frag.setArguments(args);
            return frag;
        }

        @Override
        public int getCount() {
            return mCategories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mCategories.get(position).getName();
        }
    }
}
