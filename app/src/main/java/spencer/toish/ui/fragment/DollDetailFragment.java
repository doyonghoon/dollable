package spencer.toish.ui.fragment;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.etsy.android.grid.StaggeredGridView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.melnykov.fab.FloatingActionButton;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.adapter.ExtraImageAdapter;
import spencer.toish.app.App;
import spencer.toish.model.Category;
import spencer.toish.model.Toy;
import spencer.toish.model.ExtraImage;
import spencer.toish.model.Pref;
import spencer.toish.query.ToyQuery;
import spencer.toish.ui.activity.TransparencyActivity;
import spencer.toish.ui.view.widget.DetailDollView;
import spencer.toish.util.WLog;

/**
 * Created by doyonghoon on 14. 9. 28..
 */
public class DollDetailFragment extends BaseFragment implements ImageChooserListener, AbsListView.OnScrollListener, PullRefreshLayout.OnRefreshListener {

    private Toy mToy;

    @InjectView(R.id.swipeRefreshLayout)
    PullRefreshLayout mPullRefresh;
    @InjectView(R.id.grid_view)
    StaggeredGridView mGridListView;
    @InjectView(R.id.plus)
    FloatingActionButton mAddButton;

    private DetailDollView mDetailDollView = null;
    private ExtraImageAdapter mAdapter = null;

    private List<ExtraImage> mExtraImages = new ArrayList<ExtraImage>();
    private float mInitialPlusTranslationY, mSecondPlusTranslationY = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_detail_doll_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBaseActivity().getSupportActionBar().setTitle("");

        updateUi(getStringFromBundle("dollCode"), null);
        mPullRefresh.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        mPullRefresh.setOnRefreshListener(this);
        mGridListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> paths = new ArrayList<String>();
                for (ExtraImage i : mExtraImages) {
                    paths.add(i.getPath());
                }
                Bundle b = new Bundle();
                b.putInt("current_item", position - 1);
                b.putStringArrayList("paths", paths);
                App.goTo(getActivity(), FragmentType.DOLL_IMAGE_GALLERY, false, b);
            }
        });

        mGridListView.setOnScrollListener(this);
    }

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            final float height = mDetailDollView.getCover().getHeight();
            if (height > 0) {
                ViewTreeObserver vto = mDetailDollView.getViewTreeObserver();
                if (vto.isAlive()) {
                    vto.removeGlobalOnLayoutListener(mGlobalLayoutListener);
                }

                final float buttonHeight = mAddButton.getHeight();
                mInitialPlusTranslationY = mAddButton.getTranslationY();
                mAddButton.setScaleX(0);
                mAddButton.setScaleY(0);
                mAddButton.setY(height - (buttonHeight / 2));
                mSecondPlusTranslationY = mAddButton.getTranslationY();
                mAddButton.animate().scaleX(1).scaleY(1).setDuration(270).setStartDelay(250).setInterpolator(new AccelerateInterpolator()).start();
            }
        }
    };

    /**
     * 장난감의 상세 정보를 서버에서 불러오고 화면에 보여줌.
     * @param layout 화면을 갱신할때 데이터를 불러오는 동안 보여줄 indicator. 보여주기 싫으면 Null을 입력 해도 됨.
     * @param objectId 유저의 ObjectId.
     * */
    public void updateUi(String objectId, final PullRefreshLayout layout) {
        ToyQuery query = new ToyQuery();
        query.loadToy(objectId, new GetCallback<Toy>() {
            @Override
            public void done(Toy toy, ParseException e) {
                if (e == null) {
                    mToy = toy;
                    if (mDetailDollView == null) {
                        mDetailDollView = new DetailDollView(getActivity());
                        ViewCompat.setElevation(mDetailDollView, getResources().getDimension(R.dimen.toolbar_elevation));
                        ViewTreeObserver vto = mDetailDollView.getViewTreeObserver();
                        if (vto.isAlive()) {
                            vto.addOnGlobalLayoutListener(mGlobalLayoutListener);
                        }
                        mGridListView.addHeaderView(mDetailDollView);
                    }
                    mDetailDollView.updateUi(mToy);

                    if (mAdapter == null)
                        mAdapter = new ExtraImageAdapter(getActivity(), Pref.shouldShowDeleteExtraImage(getActivity()));

                    if (mGridListView.getAdapter() == null)
                        mGridListView.setAdapter(mAdapter);

                    mAdapter.clear();
                    mExtraImages = new ArrayList<ExtraImage>();
                    if (mToy.getProfileImageFile() != null) {
                        ExtraImage profileImage = new ExtraImage();
                        profileImage.setCode(mToy.getObjectId());
                        profileImage.setImageFile(mToy.getProfileImageFile());
                        mExtraImages.add(profileImage);
                    }

                    ParseQuery<ExtraImage> query = ParseQuery.getQuery(ExtraImage.class);
                    query.whereEqualTo("code", mToy.getObjectId());
                    query.fromLocalDatastore();
                    query.findInBackground(new FindCallback<ExtraImage>() {
                        @Override
                        public void done(List<ExtraImage> extraImages, ParseException e) {
                            if (extraImages != null && !extraImages.isEmpty())
                                mExtraImages.addAll(extraImages);

                            if (e != null)
                                e.printStackTrace();

                            mAdapter.clear();
                            mAdapter.addAll(mExtraImages);
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "토이를 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }

                if (layout != null)
                    layout.setRefreshing(false);
            }
        });
    }

    private AlertDialog.Builder mDialog;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detail, menu);
//        MenuItem share = menu.findItem(R.id.action_share);
        MenuItem edit = menu.findItem(R.id.action_edit);
        MenuItem remove = menu.findItem(R.id.action_remove);
        edit.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        remove.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//        share.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToy == null) return false;
        switch (item.getItemId()) {
//            case R.id.action_share:
//                final ScreenShareView v = new ScreenShareView(getActivity());
//                v.setMainView(mDetailDollView);
//                v.setExtraImages(mExtraImages);
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                builder.setView(v);
//                builder.setPositiveButton("공유하기", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent shareIntent = new Intent();
//                        shareIntent.setAction(Intent.ACTION_SEND);
//                        shareIntent.putExtra(Intent.EXTRA_STREAM, BitmapUtil.getImageUri(getActivity(), v.takeScreenshot()));
//                        shareIntent.setType("image/jpeg");
//                        startActivity(Intent.createChooser(shareIntent, "이미지 공유하기"));
//                    }
//                });
//                builder.show();
//                return true;

            case R.id.action_edit:
                final String dollId = mToy.getObjectId();
                Bundle bundle = new Bundle();
                bundle.putString("dollCode", dollId);
                bundle.putString(Category.CATEGORY_CODE, mToy.getCategoryCode());
                bundle.putBoolean("edit", true);
                App.getDollCache().put(dollId, mToy);
                App.goTo(getActivity(), FragmentType.DOLL_ADD_OR_EDIT, bundle, false, 0, false);
                return true;

            case R.id.action_remove:
                mDialog = new AlertDialog.Builder(getActivity());
                mDialog.setTitle("삭제하기").setMessage(mToy.getName() + " 토이를 정말 삭제하시겠어요?")
                        .setNegativeButton("아니요", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showProgressDialog("삭제 중..");
                        final String name = mToy.getName();
                        ParseQuery<ExtraImage> images = ParseQuery.getQuery(ExtraImage.class);
                        images.whereEqualTo("code", mToy.getObjectId());
                        images.findInBackground(new FindCallback<ExtraImage>() {
                            @Override
                            public void done(List<ExtraImage> extraImages, ParseException e) {
                                if (e == null) {
                                    for (ExtraImage i : extraImages) {
                                        i.deleteInBackground();
                                    }
                                }

                                mToy.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            mToy.unpinInBackground(new DeleteCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    dismissDialog();
                                                    if (e == null) {
                                                        Toast.makeText(getActivity(), name + " 토이가 삭제되었습니다!", Toast.LENGTH_SHORT).show();
                                                        Intent result = new Intent();
                                                        result.putExtra(Category.CATEGORY_CODE, mToy.getCategoryCode());
                                                        getActivity().setResult(Activity.RESULT_OK, result);
                                                        getActivity().finish();
                                                    } else {
                                                        Toast.makeText(getActivity(), "삭제하지 못했습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }).show();
                return true;
        }

        return false;
    }

    private ProgressDialog mProgressDialog = null;

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);

        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    private void dismissDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @OnClick(R.id.plus)
    public void onClickExtraImage() {
        showCaptureImageDialog("사진 가져오기");
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WLog.i("height: " + chosenImage.getMediaHeight());
                if (chosenImage != null && !TextUtils.isEmpty(chosenImage.getFilePathOriginal())) {
                    showProgressDialog("사진 불러오는 중..");
                    mTempImageFile = new File((chosenImage.getFilePathOriginal()));
                    mImageCaptureUri = Uri.fromFile(mTempImageFile);
                    int aspectX = 0;
                    int aspectY = 0;
                    doCrop(aspectX, aspectY);
                } else {
                    Toast.makeText(getActivity(), "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onError(final String s) {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                dismissDialog();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null && data.hasExtra("dollCode")) {
            updateUi(data.getStringExtra("dollCode"), null);
            getActivity().setResult(Activity.RESULT_OK);
            return;
        }

        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == REQUEST_CROP && mImageCaptureUri != null) {
            final ExtraImage i = new ExtraImage();
            i.setCode(mToy.getObjectId());
            i.setImagePath(mImageCaptureUri.getPath());
            i.getImageFile().saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    showProgressDialog("사진 저장 중..");
                    i.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                i.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        dismissDialog();
                                        updateUi(getStringFromBundle("dollCode"), null);
                                    }
                                });
                            } else {
                                dismissDialog();
                                Toast.makeText(getActivity(), "저장하지 못했습니다", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
            return;
        }

        if (requestCode == ChooserType.REQUEST_CAPTURE_PICTURE || requestCode == ChooserType.REQUEST_PICK_PICTURE) {
            if (mImageChooserManager == null)
                reinitializeImageChooser();

            mImageChooserManager.submit(requestCode, data);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mGridListView.getHeaderViewsCount() >= 1) {
            if (firstVisibleItem <= mGridListView.getHeaderViewsCount()) {
                final int offsetValue = (getCoverHeight() / 2) - getScrollY();
                TransparencyActivity act = (TransparencyActivity) getActivity();
                boolean isVisible = false;
                if (App.getAlphaCache().get("alpha") != null) {
                    isVisible = App.getAlphaCache().get("alpha") == 1.0f;
                } else {
                    App.getAlphaCache().put("alpha", 1.0f);
                }
                if (offsetValue > 0) {
                    if (isVisible) {
                        act.animateHideToolbar();
                    }
                } else {
                    if (!isVisible) {
                        act.animateVisibleToolbar();
                    }
                }

                if (getScrollY() < 30) {
                    movePlusButtonToTop();
                } else {
                    movePlusButtonToBottom();
                }
            }
        }
    }

    private boolean isInitialAnimating = false, isSecondAnimating = false;

    private void movePlusButtonToBottom() {
        if (isInitialAnimating) return;
        if (mAddButton.getTranslationY() != mInitialPlusTranslationY)
            mAddButton.animate().translationY(mInitialPlusTranslationY).setDuration(280).setStartDelay(50).setInterpolator(new AccelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    isInitialAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    isInitialAnimating = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    isInitialAnimating = false;
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
    }

    private void movePlusButtonToTop() {
        if (isSecondAnimating) return;
        if (mAddButton.getTranslationY() != mSecondPlusTranslationY)
            mAddButton.animate().translationY(mSecondPlusTranslationY).setDuration(280).setStartDelay(50).setInterpolator(new AccelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    isSecondAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    isSecondAnimating = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    isInitialAnimating = false;
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
    }

    private int getCoverHeight() {
        return mDetailDollView.getCover().getHeight();
    }

    private int getScrollY() {
        final View c = mGridListView.getChildAt(0);
        return -c.getTop() + mGridListView.getFirstVisiblePosition() * c.getHeight();
    }

    @Override
    public void onRefresh() {
        updateUi(getStringFromBundle("dollCode"), mPullRefresh);
        mPullRefresh.setRefreshing(true);
    }
}
