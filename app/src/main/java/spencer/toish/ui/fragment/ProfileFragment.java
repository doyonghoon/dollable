package spencer.toish.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.File;
import java.util.List;

import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.adapter.HeaderViewRecyclerAdapter;
import spencer.toish.adapter.OwnerProfileAdapter;
import spencer.toish.model.Toy;
import spencer.toish.model.OwnerProfileMenu;
import spencer.toish.query.ToyQuery;
import spencer.toish.ui.activity.LoginActivity;
import spencer.toish.ui.view.widget.ChangeFieldView;
import spencer.toish.ui.view.widget.ProfileHeaderView;
import spencer.toish.util.BitmapUtil;

/**
 * Created by doyonghoon on 14. 11. 17..
 */
public class ProfileFragment extends BaseFragment implements OwnerProfileAdapter.OnProfileItemClickListener, ImageChooserListener {

    @InjectView(R.id.recycler_view) public RecyclerView mRecyclerView;

    private OwnerProfileAdapter mOwnerAdapter;
    private HeaderViewRecyclerAdapter mAdapter;
    private LinearLayoutManager mOwnerManager;
    private ProfileHeaderView mHeaderView;

    private String mOwnerCode = null;

    private ProgressDialog mProgressDialog = null;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_profile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseBundle();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBaseActivity().getSupportActionBar().setTitle(null);
        updateUi();
    }

    private void updateUi() {
        if (mOwnerManager == null) {
            // initialize..
            mRecyclerView.setHasFixedSize(false);
            mOwnerManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            mRecyclerView.setLayoutManager(mOwnerManager);
        }

        if (mAdapter == null) {
            mOwnerAdapter = new OwnerProfileAdapter(getActivity());
            mOwnerAdapter.setOnClickListener(this);
            mAdapter = new HeaderViewRecyclerAdapter(mOwnerAdapter);
        }

        if (mHeaderView == null) {
            mHeaderView = new ProfileHeaderView(getActivity());
            mAdapter.addHeaderView(mHeaderView);
        }

        mHeaderView.setOwner(ParseUser.getCurrentUser());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void parseBundle() {
        Bundle b = getArguments();
        if (b.containsKey("ownerCode"))
            mOwnerCode = b.getString("ownerCode");
    }

    private boolean isMe() {
        return !TextUtils.isEmpty(mOwnerCode) && mOwnerCode.equals(ParseUser.getCurrentUser().getObjectId());
    }

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
        }

        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        mProgressDialog.setMessage(message);

        mProgressDialog.show();
    }

    private void dismissDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    private void showChangeFieldDialog(String title, String subtitle, String hint, boolean isPassword, final ChangeFieldView.OnChangeFieldListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);

        final ChangeFieldView v = new ChangeFieldView(getActivity());
        builder.setView(v);
        v.setTitle(subtitle);
        v.setHint(hint);
        if (isPassword)
            v.setPassword();
        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("바꾸기", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onChange(v.getValue());
                }
            }
        });
        builder.show();
    }

    @Override
    public void onClick(OwnerProfileMenu menu) {
        switch (menu) {
            case CHANGE_PROFILE_IMAGE:
                showCaptureImageDialog("내 프로필 사진 가져오기");
                break;

            case CHANGE_NICKNAME:
                showChangeFieldDialog("닉네임 바꾸기", "닉네임을 바꿉니다", "닉네임", false, new ChangeFieldView.OnChangeFieldListener() {
                    @Override
                    public void onChange(final String value) {
                        if (!TextUtils.isEmpty(value)) {
                            showProgressDialog("닉네임을 " + value + "로 변경 중..");
                            ParseUser user = ParseUser.getCurrentUser();
                            user.put("nickname", value);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    dismissDialog();
                                    if (e == null) {
                                        ParseUser.getCurrentUser().put("nickname", value);
                                        updateUi();
                                    } else {
                                        Toast.makeText(getActivity(), "닉네임을 변경하지 못했습니다", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(getActivity(), "빈칸으로 변경할 수는 없어요", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;

            case CHANGE_PASSWORD:
                showChangeFieldDialog("비밀번호 바꾸기", "비밀번호를 바꿉니다", "비밀번호", true, new ChangeFieldView.OnChangeFieldListener() {
                    @Override
                    public void onChange(final String value) {
                        if (!TextUtils.isEmpty(value)) {
                            showProgressDialog("비밀번호 변경 중..");
                            ParseUser user = ParseUser.getCurrentUser();
                            user.setPassword(value);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    dismissDialog();
                                    if (e == null) {
                                        Toast.makeText(getActivity(), "비밀번호가 변경되었습니다!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getActivity(), "비밀번호를 변경하지 못했습니다", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(getActivity(), "빈칸으로 변경할 수는 없어요", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;

            case RESET_DOLL:
                AlertDialog.Builder resetDollBuilder = new AlertDialog.Builder(getActivity());
                resetDollBuilder.setTitle("토이 모두 삭제하기");
                resetDollBuilder.setMessage("한 번 삭제된 토이는 다시 되돌릴 수 없어요. 정말 삭제 하시겠어요?");
                resetDollBuilder.setPositiveButton("진짜 삭제하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ToyQuery query = new ToyQuery();
                        query.loadToys(ParseUser.getCurrentUser().getObjectId(), null, false, new FindCallback<Toy>() {
                            @Override
                            public void done(List<Toy> toys, ParseException e) {
                                if (e == null) {
                                    for (Toy d : toys)
                                        d.deleteInBackground();
                                    Toast.makeText(getActivity(), "곧 모든 토이가 삭제됩니다!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), "삭제할 토이가 없거나 인터넷이 불안해 보여요", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });
                resetDollBuilder.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                resetDollBuilder.show();
                break;

//            case RESET_LINK:
//                AlertDialog.Builder resetLinkBuilder = new AlertDialog.Builder(getActivity());
//                resetLinkBuilder.setTitle("링크 초기화 하기");
//                resetLinkBuilder.setMessage("앱 설치 시, 등록되어있던 링크들도 바뀝니다. 자신이 직접 수정하거나 추가한 링크들은 모두 삭제되고 되돌릴 수 없어요. 정말 초기화를 하시겠어요?");
//                resetLinkBuilder.setPositiveButton("진짜 초기화 하기", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        LinkStore.getInstance().resetLinks(new LinkStore.LinkCallback() {
//                            @Override
//                            public void onSuccess(List<Link> links) {
//                                if (links != null && !links.isEmpty()) {
//                                    Toast.makeText(getActivity(), "링크가 초기화 되었습니다!", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(getActivity(), "링크가 초기화를 하지 못했습니다. 인터넷 문제일 수도 있으니, 다시 시도 해보세요 T_T", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//
//                            @Override
//                            public void onFail(ParseException e) {
//                                Toast.makeText(getActivity(), "링크가 초기화를 하지 못했습니다. 인터넷 문제일 수도 있으니, 다시 시도 해보세요 T_T", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//                });
//                resetLinkBuilder.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                resetLinkBuilder.show();
//                break;

            case LOGOUT:
                ParseUser.logOut();
                Intent i = new Intent(getActivity(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseActivity().startActivity(i);
                getBaseActivity().finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == REQUEST_CROP && mImageCaptureUri != null) {
            showProgressDialog("사진 저장 중..");
            saveProfileImageInBackground(mImageCaptureUri.getPath(), new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    dismissDialog();
                    if (e == null) {
                        updateUi();
                    } else {
                        Toast.makeText(getActivity(), "프로필 사진을 저장하지 못했습니다", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return;
        }

        if (requestCode == ChooserType.REQUEST_CAPTURE_PICTURE || requestCode == ChooserType.REQUEST_PICK_PICTURE) {
            if (mImageChooserManager == null)
                reinitializeImageChooser();

            mImageChooserManager.submit(requestCode, data);
        }
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (chosenImage != null) {
                    showProgressDialog("사진 불러오는 중..");
                    mTempImageFile = new File((chosenImage.getFilePathOriginal()));
                    mImageCaptureUri = Uri.fromFile(mTempImageFile);
                    int aspectX = 0;
                    int aspectY = 0;
                    doCrop(aspectX, aspectY);
                } else {
                    Toast.makeText(getActivity(), "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onError(String s) {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                dismissDialog();
            }
        });
    }

    private void saveProfileImageInBackground(String imagePath, final SaveCallback callback) {
        final ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            showProgressDialog("사진 저장 중..");
            ParseFile f = BitmapUtil.convertImageFileToParseFile(imagePath);
            user.put("image_file", f);
            f.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        user.saveInBackground(callback);
                    } else {
                        callback.done(e);
                    }
                }
            });
        } else {
            Toast.makeText(getActivity(), "유저 정보가 잘못 되었거나, 로그아웃된 유저라서 프로필 사진을 저장할 수 없습니다", Toast.LENGTH_LONG).show();
        }
    }
}
