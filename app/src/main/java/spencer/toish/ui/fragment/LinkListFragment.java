package spencer.toish.ui.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import spencer.toish.R;
import spencer.toish.adapter.LinkAdapter;
import spencer.toish.model.Link;
import spencer.toish.model.LinkStore;
import spencer.toish.model.Pref;
import spencer.toish.util.KoreanAlphabetSearcher;
import spencer.toish.util.WLog;

/**
 * 구매 가능한 웹사이트 목록
 */
public class LinkListFragment extends BaseFragment implements DialogInterface.OnDismissListener {

    @InjectView(R.id.sticky_list) StickyListHeadersListView mStickyList;
    @InjectView(R.id.add_link) FloatingActionButton mButton;

    private int mPreviousScrollY;
    private int mPreviousFirstVisibleItem;
    public int mLastChangeY;
    private int mMinSignificantScroll;

    private List<Link> mLinks = new ArrayList<Link>();
    private LinkAdapter mAdapter;
    private SearchDesignerWatcher mWatcher;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_list_links;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionbar = getActionbar();
        if (actionbar != null) {
            actionbar.setTitle(R.string.title_link_list);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStickyList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int newScrollY = estimateScrollY();
                if (isSameRow(firstVisibleItem) && isSignificantDelta(newScrollY)) {
                    if (isScrollUp(newScrollY)) {
                        mButton.hide();
                    } else {
                        mButton.show();
                    }
                }
            }
        });
        updateUi();
    }

    private boolean isScrollUp(int newScrollY) {
        boolean scrollUp = newScrollY > mPreviousScrollY;
        mPreviousScrollY = newScrollY;
        return scrollUp;
    }

    private boolean isSignificantDelta(int newScrollY) {
        boolean isSignificantDelta = Math.abs(mLastChangeY - newScrollY) > mMinSignificantScroll;
        if (isSignificantDelta)
            mLastChangeY = newScrollY;
        return isSignificantDelta;
    }

    private boolean isSameRow(int firstVisibleItem) {
        boolean rowsChanged = firstVisibleItem == mPreviousFirstVisibleItem;
        mPreviousFirstVisibleItem = firstVisibleItem;
        return rowsChanged;
    }

    private int estimateScrollY() {
        if (mStickyList == null || mStickyList.getChildAt(0) == null) return 0;
        View topChild = mStickyList.getChildAt(0);
        return mStickyList.getFirstVisiblePosition() * topChild.getHeight() - topChild.getTop();
    }

    @OnClick(R.id.add_link)
    public void onClickAddLink() {
        LinkEditDialogFragment dialogFragment = new LinkEditDialogFragment();
        Bundle b = new Bundle();
        dialogFragment.setArguments(b);
        getFragmentManager();
//        dialogFragment.show(getFragmentManager(), "link_create");
    }

    public void updateUi() {
        LinkStore.getInstance().fetchAllLinks(new LinkStore.LinkCallback() {
            @Override
            public void onSuccess(List<Link> links) {
                mLinks = new ArrayList<Link>(links);
                categorizeByType();
                if (mAdapter == null) {
                    mAdapter = new LinkAdapter(getActivity(), getFragmentManager(), Pref.shouldShowEditLinkTip(getActivity()));
                    @SuppressLint("InflateParams") View searchViewFrame = LayoutInflater.from(getActivity()).inflate(R.layout.view_search, null);
                    EditText searchView = ButterKnife.findById(searchViewFrame, R.id.search);
                    mWatcher = new SearchDesignerWatcher();
                    searchView.addTextChangedListener(mWatcher);
                    mWatcher.setDesigners(mLinks);
                    mWatcher.setAdapter(mAdapter);
                    mStickyList.addHeaderView(searchViewFrame);

                    mStickyList.setAdapter(mAdapter);
                    mAdapter.addAll(mLinks);
                    mAdapter.notifyDataSetChanged();

                } else {
                    mAdapter.clear();
                    mAdapter.addAll(mLinks);
                    mAdapter.notifyDataSetChanged();
                    mWatcher.setDesigners(mLinks);
                    mWatcher.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFail(ParseException e) {
                WLog.i("failed to load sites.. " + e.getMessage());
                Toast.makeText(getActivity(), "목록을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void categorizeByType() {
        List<Link> hairs = new ArrayList<Link>(getLinksByType(Link.LinkType.HAIR));
        List<Link> dolls = new ArrayList<Link>(getLinksByType(Link.LinkType.DOLL));
        List<Link> clothes = new ArrayList<Link>(getLinksByType(Link.LinkType.CLOTHES));
        Collections.sort(hairs, mLinkComparator);
        Collections.sort(dolls, mLinkComparator);
        Collections.sort(clothes, mLinkComparator);

        mLinks = new ArrayList<Link>();
        mLinks.addAll(dolls);
        mLinks.addAll(clothes);
        mLinks.addAll(hairs);
    }

    private Comparator<Link> mLinkComparator = new Comparator<Link>() {
        @Override
        public int compare(Link lhs, Link rhs) {
            return lhs.getTitle().compareToIgnoreCase(rhs.getTitle());
        }
    };

    private List<Link> getLinksByType(Link.LinkType type) {
        List<Link> links = new ArrayList<Link>();
        for (Link l : mLinks)
            if (l.getType() == type)
                links.add(l);
        return links;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        updateUi();
    }

    private static class SearchDesignerWatcher implements TextWatcher {

        private List<Link> mDesigners;
        private LinkAdapter mAdapter;

        public void setDesigners(List<Link> list) {
            mDesigners = list;
        }

        public void setAdapter(LinkAdapter adapter) {
            mAdapter = adapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!TextUtils.isEmpty(s)) {
                List<Link> temp = new ArrayList<Link>();

                for (Link d : mDesigners) {
                    if (d.containsName(s.toString().toLowerCase()) || KoreanAlphabetSearcher.matchString(d.getTitle(), s.toString()))
                        temp.add(d);
                }

                if (!temp.isEmpty()) {
                    mAdapter.clear();
                    mAdapter.addAll(temp);
                }
            } else {
                mAdapter.clear();
                mAdapter.addAll(mDesigners);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
