package spencer.toish.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.ui.view.widget.HackyViewPager;
import spencer.toish.util.StorageUtil;
import uk.co.senab.photoview.PhotoView;

/**
 * 이미지 크게보기 화면
 */
public class DollImageGalleryFragment extends BaseFragment {

    private static final String ISLOCKED_ARG = "isLocked";

    private ArrayList<String> mPaths = new ArrayList<String>();
    private int mCurrentItem = 0;

    @InjectView(R.id.pager)
    HackyViewPager mPager;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_gallery;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        mPaths = b.getStringArrayList("paths");
        mCurrentItem = b.getInt("current_item");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (isViewPagerActive()) {
            outState.putBoolean(ISLOCKED_ARG, (mPager).isLocked());
        }
    }

    private boolean isViewPagerActive() {
        return (mPager != null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final SamplePagerAdapter adapter = new SamplePagerAdapter(getActivity(), mPaths);
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(mCurrentItem);
        mPager.setOffscreenPageLimit(5);

        if (savedInstanceState != null) {
            boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG, false);
            mPager.setLocked(isLocked);
        }
    }

    static class SamplePagerAdapter extends PagerAdapter {

        private Context mContext;
        private ArrayList<String> mPaths;

        public SamplePagerAdapter(Context context, ArrayList<String> paths) {
            mPaths = paths;
            mContext = context;
        }

        @Override
        public int getCount() {
            return mPaths.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            if (mPaths != null && !mPaths.isEmpty()) {
                Picasso.with(mContext).load(mPaths.get(position)).into(photoView);
                container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                photoView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
//                        exportBitmapAsFile(file);
                        return true;
                    }
                });
            } else {
                photoView.setImageBitmap(null);
            }

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private void exportBitmapAsFile(File file) {
            StorageUtil.save(mContext, file);
        }

//        private void showDialog() {
//            final String[] sizes = getResources().getStringArray(R.array.sizes);
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Choose size");
//            builder.setItems(sizes, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    String url = null;
//                    switch (which) {
//                        case 0:
//                            url = mRunways.get(mPager.getCurrentItem()).getLargeUrl();
//                            break;
//                        case 1:
//                            url = mRunways.get(mPager.getCurrentItem()).getMediumUrl();
//                            break;
//                        case 2:
//                            url = mRunways.get(mPager.getCurrentItem()).getSmallUrl();
//                            break;
//                        case 3:
//                            url = mRunways.get(mPager.getCurrentItem()).getXSmallUrl();
//                            break;
//                    }
//
//                    trackEvent(ShowAnalytics.CATEGORY_UI_ACTION, ShowAnalytics.ACTION_FULLSCREEN_EXPORT_PRESS, sizes[which]);
//
//                    ExternalStorage.save(FullscreenActivity.this, RequestHelper.DOMAIN_IMAGE + url);
//                }
//            });
//
//            builder.show();
//        }
    }
}
