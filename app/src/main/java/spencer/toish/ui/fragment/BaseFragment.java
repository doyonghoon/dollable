package spencer.toish.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import java.io.File;
import java.util.List;

import butterknife.ButterKnife;
import me.drakeet.materialdialog.MaterialDialog;
import spencer.toish.R;
import spencer.toish.ui.activity.BaseActivity;

/**
 * Butterknife, Crashlytics, Logging Analytics, Account, onActivityResult 등을 관리하거나 초기화 함.
 */
public abstract class BaseFragment extends Fragment {

    public static final String ARG_BASE_NUMBER = "base_number";

    /**
     * ImageChooserManager 를 통해서 이미지를 가져올때 임시로 저장할 파일 경로.
     */
    protected String mImageFilePath;

    /**
     * ImageChooserManager 를 통해서 이미지를 가져올떄 사진찍기와 앨범선택 둘 중 하나의 값.
     */
    protected int mChooserType;

    /**
     * 이미지를 내부 저장소에서 가져오거나 직접 찍기로 가져오기 위한 라이브러리.
     */
    protected ImageChooserManager mImageChooserManager;

    /**
     * 이미지를 앱으로 가져올때, 이미지 파일의 임시 경로.
     * 이미지 자르기 앱으로 보내거나 받을때 이 uri의 이미지가 바뀜.
     */
    protected static Uri mImageCaptureUri;

    /**
     * 이미지를 서버로 보낼때 사용할 파일.
     * ProfileFragment, TabInfoRidingFragment 에서 사용함.
     */
    protected File mTempImageFile;

    public static BaseFragment newInstance(FragmentType type, Bundle b) {
        BaseFragment fragment = getBaseFragment(type);
        Bundle args = b != null ? new Bundle(b) : new Bundle();
        args.putInt(ARG_BASE_NUMBER, type.getArg());
        fragment.setArguments(args);
        return fragment;
    }

    @LayoutRes protected abstract int getLayoutId();

    public ActionBar getActionbar() {
        if (getActivity() != null) {
            BaseActivity act = (BaseActivity) getActivity();
            return act.getSupportActionBar();
        }
        return null;
    }

    public Toolbar getToolbar() {
        if (getActivity() != null) {
            BaseActivity act = (BaseActivity) getActivity();
            return act.getToolbar();
        }
        return null;
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    /**
     * 이미지를 내부 저장소에서 꺼내올지, 직접 찍을지 유저에게 선택 권한을 주는 다이얼로그를 띄움.
     * index[0] 사진찍기
     * index[1] 앨범에서 선택
     */
    private MaterialDialog mDialog;
    protected void showCaptureImageDialog(String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setItems(R.array.titles_capture_image, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mChooserType = which == 0 ? ChooserType.REQUEST_CAPTURE_PICTURE : ChooserType.REQUEST_PICK_PICTURE;
                    reinitializeImageChooser();
                    mImageFilePath = mImageChooserManager.choose();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        builder.show();
    }

    protected void reinitializeImageChooser() {
        mImageChooserManager = new ImageChooserManager(this, mChooserType, "dollable", true);
        if (this instanceof ImageChooserListener) {
            mImageChooserManager.setImageChooserListener((ImageChooserListener) this);
            mImageChooserManager.reinitialize(mImageFilePath);
        }
    }

    protected final static int REQUEST_CROP = 2;

    /**
     * 가져온 사진을 자를 수 있는 외부 앱으로 보냄.
     */
    public static final int COVER_ASPECT_X = 1000;
    public static final int COVER_ASPECT_Y = 500;

    protected void doCrop(int aspectX, int aspectY) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(mImageCaptureUri, "image/*");
        List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(intent, 0);
        if (list != null && !list.isEmpty()) {
            intent.putExtra("return-data", false);
            intent.putExtra("scale", true);
            if (aspectX > 0 && aspectY > 0) {
                intent.putExtra("aspectX", aspectX);
                intent.putExtra("aspectY", aspectY);
                intent.putExtra("outputX", aspectX);
                intent.putExtra("outputY", aspectY);
            }
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            getActivity().startActivityForResult(intent, REQUEST_CROP);
        }
    }

    protected String getStringFromBundle(String key) {
        if (getArguments() != null && getArguments().containsKey(key)) {
            return getArguments().getString(key);
        }
        return null;
    }

    protected boolean getBooleanFromBundle(String key, boolean defaultValue) {
        if (getArguments() != null && getArguments().containsKey(key)) {
            return getArguments().getBoolean(key, defaultValue);
        }
        return defaultValue;
    }

    public static FragmentType getTypeByArg(int arg) {
        for (FragmentType t : FragmentType.values()) {
            if (t.getArg() == arg) {
                return t;
            }
        }
        return null;
    }

    public boolean canBackPressed() {
        return true;
    }

    private static BaseFragment getBaseFragment(FragmentType fragmentType) {
        switch (fragmentType) {
//            case LINK_LIST:
//                return new LinkListFragment();
            case DOLL_LIST:
                return new DollListFragment();
            case DOLL_DETAIL:
                return new DollDetailFragment();
            case DOLL_ADD_OR_EDIT:
                return new DollAddOrEditFragment();
            case DOLL_IMAGE_GALLERY:
                return new DollImageGalleryFragment();
            case OWNER_PROFILE:
                return new ProfileFragment();
            case CATEGORY_LIST:
                return new CategoryFragment();
            default:
                return null;
        }
    }

    public static enum FragmentType {
//        LINK_LIST(0x00, "link_list"),
        DOLL_LIST(0x01, "doll_list"),
        DOLL_DETAIL(0x02, "doll_detail"),
        DOLL_ADD_OR_EDIT(0x03, "doll_add_or_edit"),
        DOLL_IMAGE_GALLERY(0x04, "doll_gallery"),
        OWNER_PROFILE(0x05, "calendar"),
        CATEGORY_LIST(0x06, "category");

        private int mArg;
        private String mScreenName;

        FragmentType(int arg, String screenName) {
            mArg = arg;
            mScreenName = screenName;
        }

        public int getArg() {
            return mArg;
        }

        public String getScreenName() {
            return mScreenName;
        }
    }
}
