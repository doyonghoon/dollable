package spencer.toish.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mrengineer13.snackbar.SnackBar;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bolts.Capture;
import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.helper.DeleteBatch;
import spencer.toish.helper.SaveBatch;
import spencer.toish.model.Category;
import spencer.toish.model.Toy;
import spencer.toish.model.ExtraField;
import spencer.toish.ui.view.widget.ExtraFieldTextView;
import spencer.toish.util.BitmapUtil;
import spencer.toish.util.WLog;

/**
 * 토이를 추가하거나, 수정할 수 있는 화면
 */
public class DollAddOrEditFragment extends BaseFragment implements ImageChooserListener {
    @InjectView(R.id.edit_cover)
    ImageView mEditCover;
    @InjectView(R.id.cover_frame)
    FrameLayout mCoverFrame;
    @InjectView(R.id.name)
    EditText mName;
    @InjectView(R.id.description)
    EditText mDescription;
    @InjectView(R.id.infos_layout)
    LinearLayout mInfosLayout;
    @InjectView(R.id.cover)
    ImageView mCover;
    @InjectView(R.id.extra_field_layout)
    LinearLayout mExtraFieldLayout;

    private ProgressDialog mProgressDialog = null;

    private String mDollCode = null;
    private String mCategoryCode = null;
    private CurrentImageRequest mCurrentRequest;

    private List<ExtraField> mExtraFields = new ArrayList<ExtraField>();

    @Override
    protected int getLayoutId() {
        return R.layout.frag_doll_add_or_edit;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCategoryCode = getStringFromBundle(Category.CATEGORY_CODE);
        mDollCode = getStringFromBundle("dollCode");
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUi();
    }

    private void updateUi() {
        if (!TextUtils.isEmpty(mDollCode)) {
            Toy toy = App.getDollCache().get(mDollCode);

            ParseFile f = toy.getProfileImageFile();
            if (f != null) {
                f.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        Bitmap bm = BitmapUtil.convertBytesToBitmap(bytes);
                        mCover.setImageBitmap(bm);
                        mCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                });
            }
            mName.setText(toy.getName());
            mDescription.setText(toy.getDescription());

            ParseQuery<ExtraField> query = new ParseQuery<ExtraField>(ExtraField.class);
            query.fromLocalDatastore();
            query.whereEqualTo("code", mDollCode);
            query.fromLocalDatastore();
            query.findInBackground(new FindCallback<ExtraField>() {
                @Override
                public void done(List<ExtraField> extraFields, ParseException e) {
                    if (e == null) {
                        if (extraFields != null && !extraFields.isEmpty()) {
                            for (ExtraField f : extraFields) {
                                addExtraFieldView(f);
                            }
                        } else {
                            ParseQuery<ExtraField> q1 = new ParseQuery<ExtraField>(ExtraField.class);
                            q1.whereEqualTo("code", mDollCode);
                            q1.findInBackground(new FindCallback<ExtraField>() {
                                @Override
                                public void done(List<ExtraField> extraFields, ParseException e) {
                                    if (e == null) {
                                        if (extraFields != null && !extraFields.isEmpty()) {
                                            for (ExtraField f : extraFields) {
                                                addExtraFieldView(f);
                                            }
                                        }
                                    } else {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }

                    } else {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void addExtraFieldView(ExtraField field) {
        ExtraFieldTextView v = new ExtraFieldTextView(getActivity());
        if (field != null) {
            v.setTitle(field.getTitle());
            v.setValue(field.getValue());
        }
        v.setOnRemoveListener(new ExtraFieldTextView.OnClickRemoveListener() {
            @Override
            public void onClickRemove(ExtraFieldTextView v) {
                mExtraFieldLayout.removeView(v);
            }
        });
        mExtraFieldLayout.addView(v);
    }

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    private void save() {
        final String name = mName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getActivity(), "이름은 꼭 입력해야해요", Toast.LENGTH_SHORT).show();
            mName.requestFocus();
            return;
        }

        Toy toy = null;
        final boolean isExists = !TextUtils.isEmpty(mDollCode);
        if (isExists) {
            toy = App.getDollCache().get(mDollCode);
        } else {
            toy = new Toy();
        }

        showProgressDialog(isExists ? "수정 중.." : "저장 중..");

        toy.setCategoryCode(mCategoryCode);
        toy.setOwner();
        toy.setName(name);
        toy.setDescription(mDescription.getText().toString());

        final Capture<Toy> capture = new Capture<Toy>(toy);
        final Toy d = capture.get();

        if (mCurrentRequest != null)
            d.setProfileImage(mCurrentRequest.getUri().getPath());

        d.getProfileImageFile().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    WLog.i("saved profile image..");
                    d.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                WLog.i("pinned in local database: " + d.getObjectId());
                                d.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            WLog.i("successfully saved doll in server: " + d.getObjectId());
                                            final List<String> remainedCodes = new ArrayList<String>();
                                            for (int i = 0; i < mExtraFieldLayout.getChildCount(); i++) {
                                                ExtraFieldTextView child = (ExtraFieldTextView) mExtraFieldLayout.getChildAt(i);
                                                ExtraField field = child.getExtraField();
                                                // 토이와 관계 생성
                                                field.setCode(d.getObjectId());
                                                mExtraFields.add(field);
                                                remainedCodes.add(field.getObjectId());
                                            }

                                            DeleteBatch deleteBatch = new DeleteBatch();
                                            deleteBatch.delete(remainedCodes, d.getObjectId(), new DeleteBatch.OnDeleteCompleteListener() {
                                                @Override
                                                public void onDeleteComplete(ParseException e) {
                                                    if (e == null) {
                                                        WLog.i("successfully deleted unused extra fields..");
                                                        SaveBatch<ExtraField> batch = new SaveBatch<ExtraField>(mExtraFields);
                                                        batch.save(new SaveBatch.OnSaveCompleteListener() {
                                                            @Override
                                                            public void onSaveComplete(ParseException e) {
                                                                if (e == null) {
                                                                    WLog.i("successfully saved extra fields..");
                                                                    d.saveInBackground(new DollSaveCallback(getActivity(), mProgressDialog, d.getObjectId(), mCategoryCode));
                                                                } else {
                                                                    WLog.i("failed to save extra fields..");
                                                                    e.printStackTrace();
                                                                    Toast.makeText(getActivity(), "저장하지 못했습니다", Toast.LENGTH_SHORT).show();

                                                                    if (mProgressDialog != null)
                                                                        mProgressDialog.dismiss();
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        WLog.i("failed to delete extra fields..");
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                        } else {
                                            WLog.i("failed to save doll..");
                                            e.printStackTrace();
                                        }
                                    }
                                });


                            } else {
                                WLog.i("failed to store doll in local database..");
                                e.printStackTrace();
                            }
                        }
                    });


                } else {
                    WLog.i("failed to save profile image..");

                    e.printStackTrace();

                    if (mProgressDialog != null)
                        mProgressDialog.dismiss();
                }
            }
        });


        if (!TextUtils.isEmpty(mDollCode))
            App.getDollCache().remove(mDollCode);
    }

    @OnClick(R.id.btn_add_extra_field)
    public void onClickAddExtraField() {
        addExtraFieldView(null);
    }

    private static class DollSaveCallback extends SaveCallback {

        private String mObjectId = null;
        private String mCategoryCode = null;
        private Activity mActivity = null;
        private ProgressDialog mDialog = null;

        public DollSaveCallback(Activity act, ProgressDialog dialog, String dollObjectId, String categoryCode) {
            mActivity = act;
            mDialog = dialog;
            mObjectId = dollObjectId;
            mCategoryCode = categoryCode;
        }

        @Override
        public void done(ParseException e) {
            if (mDialog != null)
                mDialog.dismiss();

            if (e == null) {
                Intent result = new Intent();
                result.putExtra(Category.CATEGORY_CODE, mCategoryCode);
                result.putExtra("dollCode", mObjectId);
                mActivity.setResult(Activity.RESULT_OK, result);
                mActivity.finish();
            } else {
                Toast.makeText(mActivity, "문제가 생겨서 정보를 저장하지 못했어요. 인터넷이 불안정하거나, 기기의 저장공간이 꽉 찼을 수도 있으니 확인하고 다시 시도해주세요", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit, menu);
        MenuItem registerMenuItem = menu.findItem(R.id.save);
        registerMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case R.id.save:
                save();
                return true;
            default:
                return false;
        }
    }

    @OnClick(R.id.edit_cover)
    public void onClickCover() {
        showImageRequestDialog(CurrentImageRequest.PROFILE);
    }

    private void showImageRequestDialog(CurrentImageRequest request) {
        showCaptureImageDialog("이미지 선택");
        mCurrentRequest = request;
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        if (chosenImage != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTempImageFile = new File((chosenImage.getFilePathOriginal()));
                    mImageCaptureUri = Uri.fromFile(mTempImageFile);
                    int aspectX = 0;
                    int aspectY = 0;
                    switch (mCurrentRequest) {
                        case PROFILE:
                            aspectX = 800;
                            aspectY = 800;
                            break;
                    }
                    doCrop(aspectX, aspectY);
                }
            });
        }
    }

    @Override
    public void onError(final String s) {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SnackBar snackBar = new SnackBar(getActivity());
                snackBar.show("사진을 불러오지 못했습니다", s, SnackBar.Style.ALERT, SnackBar.MED_SNACK);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == REQUEST_CROP && mImageCaptureUri != null) {
            if (mCover != null && mCurrentRequest != null) {
                mCurrentRequest.setUri(mImageCaptureUri);
                switch (mCurrentRequest) {
                    case PROFILE:
                        setImageFilePath(mCover, mImageCaptureUri.getPath(), 0, 0);
                        break;
                }
            }
            return;
        }

        if (requestCode == ChooserType.REQUEST_CAPTURE_PICTURE || requestCode == ChooserType.REQUEST_PICK_PICTURE) {
            if (mImageChooserManager == null)
                reinitializeImageChooser();

            mImageChooserManager.submit(requestCode, data);
        }
    }

    private void setImageFilePath(ImageView v, String path, int widthId, int heightId) {
        if (!TextUtils.isEmpty(path)) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                RequestCreator c = Picasso.with(getActivity()).load(imgFile);
                if (widthId > 0 && heightId > 0)
                    c.resizeDimen(widthId, heightId).centerCrop();
                c.into(v);
            }
        }
    }

    private static enum CurrentImageRequest {
        PROFILE;

        private Uri uri;

        public void setUri(Uri uri) {
            this.uri = uri;
        }

        public String getPath() {
            return uri.getPath();
        }

        public File getFile() {
            return new File(getPath());
        }

        public Uri getUri() {
            return uri;
        }
    }
}
