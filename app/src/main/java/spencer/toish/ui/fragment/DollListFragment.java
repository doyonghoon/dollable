package spencer.toish.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.etsy.android.grid.StaggeredGridView;
import com.melnykov.fab.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.adapter.DollAdapter;
import spencer.toish.app.App;
import spencer.toish.model.Category;
import spencer.toish.model.Toy;
import spencer.toish.query.ToyQuery;

/**
 * 토이를 그리드 배열로 보여주는 리스트 화면
 */
public class DollListFragment extends BaseFragment implements AdapterView.OnItemClickListener, PullRefreshLayout.OnRefreshListener {

    @InjectView(R.id.grid_view)
    StaggeredGridView mGridView;
    @InjectView(R.id.empty_text)
    TextView mEmptyTextView;
    @InjectView(R.id.plus)
    FloatingActionButton mPlusButton;
    @InjectView(R.id.swipeRefreshLayout)
    PullRefreshLayout mPullRefresh;

    private String mOwnerCode = null;
    private String mCategoryCode = null;

    @Override
    protected int getLayoutId() {
        return R.layout.frag_list_dolls;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String ownerCode = getStringFromBundle("ownerCode");
        mOwnerCode = !TextUtils.isEmpty(ownerCode) ? ownerCode : ParseUser.getCurrentUser().getObjectId();
        mCategoryCode = getStringFromBundle(Category.CATEGORY_CODE);

        if (getActivity() != null && getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle("토이 목록");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mGridView.setOnItemClickListener(this);
        mPlusButton.attachToListView(mGridView);
        updateAdapter(null, false);
        mPullRefresh.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        mPullRefresh.setOnRefreshListener(this);
    }

    @OnClick(R.id.plus)
    public void onClick() {
        Bundle b = new Bundle();
        b.putString(Category.CATEGORY_CODE, mCategoryCode);
        App.goTo(getActivity(), FragmentType.DOLL_ADD_OR_EDIT, b, false, 0, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            updateAdapter(null, true);
        }
    }

    private void updateAdapter(PullRefreshLayout layout, boolean needToRefresh) {
        final DollAdapter adapter = new DollAdapter(getActivity());
        mGridView.setAdapter(adapter);
        ToyQuery query = new ToyQuery();
        query.loadToys(mOwnerCode, mCategoryCode, needToRefresh, new DollListCallback(getActivity(), adapter, mEmptyTextView, layout));
    }

    @Override
    public void onRefresh() {
        updateAdapter(mPullRefresh, true);
        mPullRefresh.setRefreshing(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toy toy = (Toy) parent.getAdapter().getItem(position);
        Bundle bundle = new Bundle();
        bundle.putString("dollCode", toy.getObjectId());
        App.goTo(getActivity(), FragmentType.DOLL_DETAIL, bundle, false, 0, true);
    }

    private static class DollListCallback extends FindCallback<Toy> {
        private Context mContext;
        private DollAdapter mAdapter;
        private TextView mEmptyTextView;
        private PullRefreshLayout mLayout;

        public DollListCallback(Context context, DollAdapter adapter, TextView textView, PullRefreshLayout layout) {
            mContext = context;
            mAdapter = adapter;
            mEmptyTextView = textView;
            mLayout = layout;
        }

        @Override
        public void done(List<Toy> toys, ParseException e) {
            if (e == null) {
                mAdapter.clear();
                mAdapter.addAll(toys);
                mAdapter.notifyDataSetChanged();
                mEmptyTextView.setVisibility(mAdapter.isEmpty() ? View.VISIBLE : View.GONE);
            } else {
                Toast.makeText(mContext, "토이 목록을 불러오지 못했습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
            }

            if (mLayout != null)
                mLayout.setRefreshing(false);
        }
    }
}
