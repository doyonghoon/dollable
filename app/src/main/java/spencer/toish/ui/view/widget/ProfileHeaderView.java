package spencer.toish.ui.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;

/**
 * Created by doyonghoon on 14. 11. 18..
 */
public class ProfileHeaderView extends RelativeLayout {

    @InjectView(R.id.name) TextView mName;
    @InjectView(R.id.image) ImageView mProfileImage;
    @InjectView(R.id.main_title) TextView mMainTitle;

    private ParseUser mUser;

    public ProfileHeaderView(Context context) {
        this(context, null, 0);
    }

    public ProfileHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProfileHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_header_profile, this);
        ButterKnife.inject(this);
    }

    public void setOwner(ParseUser user) {
        mUser = user;

        if (mUser != null) {
            mMainTitle.setText(mUser.getString("nickname"));
            ParseFile f = mUser.getParseFile("image_file");
            if (f != null)
                Picasso.with(getContext()).load(mUser.getParseFile("image_file").getUrl()).into(mProfileImage);
            else
                mProfileImage.setImageBitmap(null);
        } else {
            mName.setText(null);
            mMainTitle.setText(null);
            mProfileImage.setImageBitmap(null);
        }
    }
}
