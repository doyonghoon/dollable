package spencer.toish.ui.view.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.parse.ParseUser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.model.ExtraImage;
import spencer.toish.util.ViewUtils;

/**
 * Created by doyonghoon on 14. 11. 24..
 */
public class ScreenShareView extends ScrollView {

    @InjectView(R.id.nickname) TextView mNickName;
    @InjectView(R.id.main_image) ImageView mMainImageView;
    @InjectView(R.id.extra_layout) LinearLayout mExtraLayout;
    @InjectView(R.id.content_layout) RelativeLayout mContentLayout;

    public ScreenShareView(Context context) {
        this(context, null, 0);
    }

    public ScreenShareView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScreenShareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_screen_share, this);
        ButterKnife.inject(this);
        setNickname(ParseUser.getCurrentUser().getString("nickname") + "님의 장난감");
    }

    public void setNickname(String nickname) {
        mNickName.setText(nickname);
    }

    public void setMainView(View v) {
        mMainImageView.setImageBitmap(ViewUtils.takeScreenShot(v));
    }

    public void setExtraImages(List<ExtraImage> images) {
        List<ExtraImage> list = new ArrayList<ExtraImage>(images);
        list.remove(0);

        for (ExtraImage image : images) {
            Picasso.with(getContext())
                    .load(image.getImageFile().getUrl())
                    .placeholder(new ColorDrawable(getContext().getResources().getColor(R.color.background_floating_material_dark)))
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            ImageView v = new ImageView(getContext());
                            v.setImageBitmap(bitmap);
                            mExtraLayout.addView(v);
                            mExtraLayout.requestLayout();
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
    }

    public Bitmap takeScreenshot() {
        return ViewUtils.takeScreenShot(mContentLayout);
    }
}
