package spencer.toish.ui.view.widget;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.app.App;
import spencer.toish.model.Toy;
import spencer.toish.query.ToyQuery;
import spencer.toish.ui.fragment.BaseFragment;

/**
 * Created by doyonghoon on 14. 11. 17..
 */
public class NavHeaderView extends FrameLayout {

    @InjectView(R.id.profile_image) CircularImageView mProfileImage;
    @InjectView(R.id.name) TextView mName;
    @InjectView(R.id.doll_count) TextView mDollCount;
//    @InjectView(R.id.link_count) TextView mLinkCount;

    private ParseUser mUser;

    public NavHeaderView(Context context) {
        this(context, null, 0);
    }

    public NavHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_nav_header, this);
        ButterKnife.inject(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        updateProfile(ParseUser.getCurrentUser());
    }

    public void updateProfile(ParseUser user) {
        mUser = user;
        mProfileImage.setImageResource(R.drawable.ic_person_grey600_48dp);
        if (mUser != null) {
            ParseFile file = mUser.getParseFile("image_file");
            if (file != null) {
                Picasso.with(getContext()).load(file.getUrl()).into(mProfileImage);
            }
            mName.setText(mUser.getString("nickname"));

//            LinkStore.getInstance().fetchAllLinks(new LinkStore.LinkCallback() {
//                @Override
//                public void onSuccess(List<Link> links) {
//                    if (links != null)
//                        mLinkCount.setText(links.size() + "개의 링크 보유");
//                    else
//                        mLinkCount.setText("0개의 링크 보유");
//                }
//
//                @Override
//                public void onFail(ParseException e) {
//                    if (e != null) {
//                        mLinkCount.setText(null);
//                    }
//                }
//            });

            ToyQuery q2 = new ToyQuery();
            q2.loadToys(mUser.getObjectId(), null, false, new FindCallback<Toy>() {
                @Override
                public void done(List<Toy> toys, ParseException e) {
                    if (toys != null || !toys.isEmpty())
                        mDollCount.setText(toys.size() + "개의 토이 보유");
                    else
                        mDollCount.setText("0개의 토이 보유");
                }
            });
        } else {
            mName.setText(null);
//            mLinkCount.setText(null);
            mDollCount.setText(null);
        }
    }

    @OnClick(R.id.profile_image)
    public void onClickProfileImage() {
        onClickLayout();
    }

    @OnClick(R.id.profile_layout)
    public void onClickLayout() {
        Bundle b = new Bundle();
        String userCode = null;
        if (mUser == null) {
            if (ParseUser.getCurrentUser() != null) {
                userCode = ParseUser.getCurrentUser().getObjectId();
            }
        } else {
            userCode = mUser.getObjectId();
        }

        if (!TextUtils.isEmpty(userCode))
            b.putString("userCode", userCode);

        App.goTo(getContext(), BaseFragment.FragmentType.OWNER_PROFILE, b, false, 0, true);
    }
}
