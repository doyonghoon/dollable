package spencer.toish.ui.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;

/**
 * Created by doyonghoon on 14. 11. 14..
 */
public class DetailTextView extends LinearLayout {

    @InjectView(R.id.title)
    TextView mTitle;

    @InjectView(R.id.subtitle)
    TextView mSubtitle;

    @InjectView(R.id.divider)
    View mDivider;

    public DetailTextView(Context context) {
        this(context, null, 0);
    }

    public DetailTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_text_detail, this);
        ButterKnife.inject(this);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.DetailTextView, defStyleAttr, 0);
        String title = attributes.getString(R.styleable.DetailTextView_detail_tv_title);
        String subtitle = attributes.getString(R.styleable.DetailTextView_detail_tv_subtitle);
        boolean showDivider = attributes.getBoolean(R.styleable.DetailTextView_detail_tv_divider, false);
        boolean singleLine = attributes.getBoolean(R.styleable.DetailTextView_detail_single_line, true);
        setTitle(title);
        setSubtitle(subtitle);
        mTitle.setSingleLine(singleLine);
        mSubtitle.setSingleLine(singleLine);
        mDivider.setVisibility(showDivider ? View.VISIBLE : View.INVISIBLE);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setSubtitle(String subtitle) {
        mSubtitle.setText(subtitle);
    }

    public void showDivider(boolean showDivider) {
        mDivider.setVisibility(showDivider ? View.VISIBLE : View.INVISIBLE);
    }
}
