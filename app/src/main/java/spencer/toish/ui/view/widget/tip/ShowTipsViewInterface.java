package spencer.toish.ui.view.widget.tip;
/**
 * @author Frederico Silva (fredericojssilva@gmail.com)
 * @date Oct 31, 2014
 */
public interface ShowTipsViewInterface {
	public void gotItClicked();
}
