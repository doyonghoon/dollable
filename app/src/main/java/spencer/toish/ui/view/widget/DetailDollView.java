package spencer.toish.ui.view.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.graphics.Palette;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;
import spencer.toish.model.Toy;
import spencer.toish.model.ExtraField;
import spencer.toish.util.BitmapUtil;

/**
 * Created by doyonghoon on 14. 11. 12..
 */
public class DetailDollView extends RelativeLayout {

    private Toy mToy;

    @InjectView(R.id.cover) ImageView mCover;
    @InjectView(R.id.cover_frame) FrameLayout mCoverFrame;
    @InjectView(R.id.name) TextView mName;
    @InjectView(R.id.description) DetailTextView mDescription;
    @InjectView(R.id.infos_layout) LinearLayout mInfosLayout;
    @InjectView(R.id.extra_field_layout) LinearLayout mExtraLayout;
    @InjectView(R.id.loading) ProgressBar mProgressBar;

    public DetailDollView(Context context) {
        this(context, null, 0);
    }

    public DetailDollView(Context context, AttributeSet attrs) {
        this(context, null, 0);
    }

    public DetailDollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, null, 0);
        inflate(getContext(), R.layout.view_detail_doll, this);
        ButterKnife.inject(this);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    public FrameLayout getCover() {
        return mCoverFrame;
    }

    public void updateUi(Toy toy) {
        mProgressBar.setVisibility(View.VISIBLE);

        mToy = toy;

        ParseFile f = toy.getProfileImageFile();
        if (f != null) {
            f.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    Bitmap bm = BitmapUtil.convertBytesToBitmap(bytes);
                    mCover.setImageBitmap(bm);
                    mCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    setDynamicColor(bm);
                }
            });
        }
        mName.setText(mToy.getName());
        mDescription.setSubtitle(mToy.getDescription());
        addExtraTextViews();

        ViewTreeObserver vto = mName.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(mGlobalLayoutListener);
        }
    }

    private void addExtraTextViews() {
        if (mExtraLayout != null && mExtraLayout.getChildCount() > 0) {
            mExtraLayout.removeAllViews();
        }

        ParseQuery<ExtraField> query = new ParseQuery<ExtraField>(ExtraField.class);
        query.whereEqualTo("code", mToy.getObjectId());
        query.findInBackground(new FindCallback<ExtraField>() {
            @Override
            public void done(List<ExtraField> extraFields, ParseException e) {
                if (e == null) {
                    if (extraFields != null && !extraFields.isEmpty()) {
                        int count = 1;
                        for (ExtraField f : extraFields) {
                            DetailTextView v = new DetailTextView(getContext());
                            v.setTitle(f.getTitle());
                            v.setSubtitle(f.getValue());
                            v.showDivider(count == extraFields.size());
                            mExtraLayout.addView(v);
                            count++;
                        }
                    }
                } else {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "상세 정보를 모두 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setDynamicColor(Bitmap bm) {
        mName.setBackgroundColor(getResources().getColor(R.color.theme_primary));
        mName.setTextColor(Color.WHITE);
        Palette.generateAsync(bm, new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch = palette.getVibrantSwatch();
                if (swatch != null) {
                    mName.setBackgroundColor(swatch.getRgb());
                    mName.setTextColor(swatch.getTitleTextColor());
                }
            }
        });
    }

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            ViewTreeObserver vto = mName.getViewTreeObserver();
            if (vto.isAlive()) {
                vto.removeGlobalOnLayoutListener(mGlobalLayoutListener);
            }
        }
    };
}
