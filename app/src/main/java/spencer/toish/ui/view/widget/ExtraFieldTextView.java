package spencer.toish.ui.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import spencer.toish.R;
import spencer.toish.model.ExtraField;

/**
 * Created by doyonghoon on 14. 11. 20..
 */
public class ExtraFieldTextView extends RelativeLayout {

    public interface OnClickRemoveListener {
        public void onClickRemove(ExtraFieldTextView v);
    }

    @InjectView(R.id.title) TextView mTitleView;
    @InjectView(R.id.value) TextView mValueView;

    private OnClickRemoveListener mListener;

    public ExtraFieldTextView(Context context) {
        this(context, null, 0);
    }

    public ExtraFieldTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExtraFieldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_extra_field, this);
        ButterKnife.inject(this);
    }

    public void setOnRemoveListener(OnClickRemoveListener listener) {
        mListener = listener;
    }

    public ExtraField getExtraField() {
        ExtraField field = new ExtraField();
        field.setTitle(getTitle());
        field.setValue(getValue());
        return field;
    }

    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    public void setValue(String value) {
        mValueView.setText(value);
    }

    public String getTitle() {
        return mTitleView.getText().toString();
    }

    public String getValue() {
        return mValueView.getText().toString();
    }

    @OnClick(R.id.remove)
    public void remove() {
        if (mListener != null)
            mListener.onClickRemove(this);
    }
}
