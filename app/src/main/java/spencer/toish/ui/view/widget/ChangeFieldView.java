package spencer.toish.ui.view.widget;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import spencer.toish.R;

/**
 * Created by doyonghoon on 14. 11. 18..
 */
public class ChangeFieldView extends LinearLayout {

    public interface OnChangeFieldListener {
        public void onChange(String value);
    }

    @InjectView(R.id.change_text) MaterialEditText mField;
    @InjectView(R.id.title)TextView mTitle;

    public ChangeFieldView(Context context) {
        this(context, null, 0);
    }

    public ChangeFieldView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChangeFieldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_change_edittext, this);
        ButterKnife.inject(this);
    }

    public String getValue() {
        return mField.getText().toString();
    }

    public void setHint(String hint) {
        mField.setHint(hint);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setPassword() {
        mField.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }
}
